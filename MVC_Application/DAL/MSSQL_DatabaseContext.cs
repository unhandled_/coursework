using System;
using System.Data.Entity;
using BusinessEntities;

namespace MSSQL_DAL
{
    /// <summary>
    /// Database context for current business entities.
    /// </summary>
    public class DataContext : DbContext
    {
        /// <summary>
        /// Creates and initializes database context with connection String.
        /// </summary>
        /// <param name="nameOrConnectionString">String used for connection to database.</param>
        public DataContext(String nameOrConnectionString) : base(nameOrConnectionString)
        {
            
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>()
                .HasMany(e => e.SolvedTasks)
                .WithMany(set => set.UsersSolved)
                .Map(mc =>
                    {
                        mc.ToTable("SolvedTasksToUsers");
                        mc.MapLeftKey("UserId");
                        mc.MapRightKey("SolvedTaskId");
                    });
            base.OnModelCreating(modelBuilder);
        }

        /// <summary>
        /// Default parameterless constructor.
        /// </summary>
        public DataContext()
        {
        }

        /// <summary>
        /// Set of users.
        /// </summary>
        public DbSet<User> Users { get; set; }

        /// <summary>
        /// Set of tasks.
        /// </summary>
        public DbSet<Task> Tasks { get; set; }

        /// <summary>
        /// Set of tags.
        /// </summary>
        public DbSet<Tag> Tags { get; set; }

        /// <summary>
        /// Set of languages.
        /// </summary>
        public DbSet<Language> Languages { get; set; }

        /// <summary>
        /// Set of user roles.
        /// </summary>
        public DbSet<Role> Roles { get; set; }

        /// <summary>
        /// Set of drafts.
        /// </summary>
        public DbSet<Draft> Drafts { get; set; }

        /// <summary>
        /// Set of technologies.
        /// </summary>
        public DbSet<Technology> Technologies { get; set; }

        /// <summary>
        /// Set of answers.
        /// </summary>
        public DbSet<Answer> Answers { get; set; }

        /// <summary>
        /// Set of likes.
        /// </summary>
        public DbSet<Like> Likes { get; set; }
    }
}