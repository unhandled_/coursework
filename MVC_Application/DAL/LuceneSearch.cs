﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.IO;
using System.Linq;
using DAL_Interfaces;
using Lucene.Net.Analysis.Snowball;
using Lucene.Net.Analysis.Standard;
using Lucene.Net.Documents;
using Lucene.Net.Index;
using Lucene.Net.QueryParsers;
using Lucene.Net.Search;
using Lucene.Net.Store;
using Version = Lucene.Net.Util.Version;

namespace MSSQL_DAL
{
    /// <summary>
    /// Class for fast fulltext search.
    /// </summary>
    public class LuceneSearch
    {
        /// <summary>
        /// Gets or sets the version.
        /// </summary>
        /// <value>
        /// The version of Lucene.
        /// </value>
        public Version Version { get; set; }

        /// <summary>
        /// Gets or sets the index path.
        /// </summary>
        /// <value>
        /// Folder where index files are.
        /// </value>
        public String IndexPath { get; set; }

        private FSDirectory directoryTemp;

        private FSDirectory directory
        {
            get
            {
                if (directoryTemp == null)
                {
                    directoryTemp = FSDirectory.Open(new DirectoryInfo(IndexPath));
                }
                if (IndexWriter.IsLocked(directoryTemp))
                {
                    IndexWriter.Unlock(directoryTemp);
                }
                String lockFilePath = Path.Combine(IndexPath, "write.lock");
                if (File.Exists(lockFilePath))
                {
                    File.Delete(lockFilePath);
                }
                return directoryTemp;
            }
        }

        public IEnumerable<Guid> FindItems(string query, params string[] fields)
        {
            IndexSearcher searcher = null;
            try
            {
                searcher = new IndexSearcher(directory, true);
                return GetItems(query, searcher, fields).ToList();
            }
            finally
            {
                if (searcher != null)
                {
                    searcher.Dispose();
                }
            }
        }

        public LuceneSearch(Version version = Version.LUCENE_30)
        {
            Version = version;
            IndexPath = String.Format("{0}{1}",
                                      AppDomain.CurrentDomain.BaseDirectory,
                                      ConfigurationManager
                                          .ConnectionStrings["LuceneIndexRelativeFolder"]
                                          .ConnectionString);
        }

        public void AddToLuceneIndex(IEnumerable<Document> documents)
        {
            using (StandardAnalyzer analyzer = new StandardAnalyzer(Version))
            {
                using (IndexWriter writer = new IndexWriter(directory, analyzer, IndexWriter.MaxFieldLength.UNLIMITED))
                {
                    foreach (var document in documents)
                    {
                        RemoveOldDocuments(document.GetValues("Id")[0], writer);
                        writer.AddDocument(document);
                    }
                    writer.Optimize();
                }
            }
        }

        public void AddToLuceneIndex(Document document)
        {
            using (var analyzer = new StandardAnalyzer(Version))
            {
                using (IndexWriter writer = new IndexWriter(directory, analyzer, IndexWriter.MaxFieldLength.UNLIMITED))
                {
                    RemoveOldDocuments(document.GetValues("Id")[0], writer);
                    writer.AddDocument(document);
                    writer.Optimize();
                }
            }
        }

        public void RemoveFromLuceneIndex(Document document)
        {
            using (StandardAnalyzer analyzer = new StandardAnalyzer(Version))
            {
                using (IndexWriter writer = new IndexWriter(directory, analyzer, IndexWriter.MaxFieldLength.UNLIMITED))
                {
                    RemoveOldDocuments(document.GetValues("Id").First(), writer);
                }
            }
        }

        private void RemoveOldDocuments(String idValue, IndexWriter writer)
        {
            var searchQuery = new TermQuery(new Term("Id", idValue));
            writer.DeleteDocuments(searchQuery);
        }

        private IEnumerable<Guid> GetItems(string query, Searcher searcher, params string[] fields)
        {
            using (StandardAnalyzer analyzer = new StandardAnalyzer(Version))
            {
                var parser = new MultiFieldQueryParser(Version, fields, analyzer);
                var scoreDocs = searcher.Search(parser.Parse(query), 100).ScoreDocs;
                return scoreDocs.Select(x => Guid.Parse(searcher.Doc(x.Doc).Get("Id")));
            }
        }

    }
}  