using BusinessEntities;
using DAL_Interfaces.Repositories.Concrete;
using Lucene.Net.Documents;

namespace MSSQL_DAL.Repositories
{
    /// <summary>
    /// ITagRepository implementation for MSSQL Server.
    /// </summary>
    public class MSSQL_TagRepository: MSSQL_Repository<Tag>, ITagRepository
    {
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="database">Database that will be used to connect to server.</param>
        public MSSQL_TagRepository(MSSQL_Database database) : base(database)
        {
        }
    }
}