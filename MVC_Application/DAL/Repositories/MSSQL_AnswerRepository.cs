using BusinessEntities;
using DAL_Interfaces.Repositories.Concrete;
using Lucene.Net.Documents;

namespace MSSQL_DAL.Repositories
{
    /// <summary>
    /// IAnswerRepository implementation for MSSQL Server.
    /// </summary>
    public class MSSQL_AnswerRepository: MSSQL_Repository<Answer>, IAnswerRepository
    {
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="database">Database that will be used to connect to server.</param>
        public MSSQL_AnswerRepository(MSSQL_Database database) : base(database)
        {
        }
    }
}