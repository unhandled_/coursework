using System;
using System.Collections.Generic;
using System.Linq;
using BusinessEntities;
using DAL_Interfaces.Repositories.Concrete;
using Lucene.Net.Documents;

namespace MSSQL_DAL.Repositories
{
    /// <summary>
    /// ITaskRepository implementation for MSSQL Server.
    /// </summary>
    public class MSSQL_TaskRepository : MSSQL_Repository<Task>, ITaskRepository
    {
        /// <summary>
        /// Finds items by specified query.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <returns>Sequence of items for specified query.</returns>
        public IEnumerable<Task> Find(string query)
        {
            IEnumerable<Guid> items = Search.FindItems(query, "Name", "Text", "Description",
                                                       "Tags", "Language", "Technology", "Author");
            return Where(task => items.Contains(task.Id));
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="database">Database that will be used to connect to server.</param>
        public MSSQL_TaskRepository(MSSQL_Database database)
            : base(database)
        {
        }

        /// <summary>
        /// Find task by it's link name.
        /// </summary>
        /// <param name="linkName"></param>
        /// <returns>Task with link name or null.</returns>
        public Task FindByLinkName(String linkName)
        {
            return FirstOrDefault(t => t.NameForLink == linkName);
        }

        internal override Document GetDocument(Task item)
        {
            Document result = new Document();
            result.Add(new Field("Id", item.Id.ToString(),
                                 Field.Store.YES, Field.Index.NOT_ANALYZED));
            result.Add(new Field("Name", item.Name ?? string.Empty,
                                 Field.Store.YES, Field.Index.ANALYZED,
                                 Field.TermVector.WITH_POSITIONS_OFFSETS));
            result.Add(new Field("Description", item.Description ?? string.Empty,
                                 Field.Store.YES, Field.Index.ANALYZED,
                                 Field.TermVector.WITH_POSITIONS_OFFSETS));
            result.Add(new Field("Text", item.Text ?? string.Empty,
                                 Field.Store.YES, Field.Index.ANALYZED,
                                 Field.TermVector.WITH_POSITIONS_OFFSETS));
            result.Add(new Field("Author", item.Author.Login ?? string.Empty,
                                 Field.Store.YES, Field.Index.ANALYZED,
                                 Field.TermVector.WITH_POSITIONS_OFFSETS));
            result.Add(new Field("Technology", item.Technology.Name ?? string.Empty,
                                 Field.Store.YES, Field.Index.ANALYZED,
                                 Field.TermVector.WITH_POSITIONS_OFFSETS));
            result.Add(new Field("Tags", GetTagsString(item) ?? string.Empty,
                                 Field.Store.YES, Field.Index.ANALYZED,
                                 Field.TermVector.WITH_POSITIONS_OFFSETS));
            result.Add(new Field("Language", item.Language.Name ?? string.Empty,
                                 Field.Store.YES, Field.Index.ANALYZED,
                                 Field.TermVector.WITH_POSITIONS_OFFSETS));
            return result;
        }

        private static string GetTagsString(Task item)
        {
            String tagsString = item.Tags
                .Aggregate(String.Empty, (current, tag) =>
                                         string.Format("{0} {1}", current, ("\"" + tag.Text + "\"")));
            return tagsString;
        }
    }
}