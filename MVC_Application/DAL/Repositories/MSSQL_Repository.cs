using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using DAL_Interfaces;
using DAL_Interfaces.Repositories.Basic;
using System.Data.Entity.Migrations;
using Lucene.Net.Documents;

namespace MSSQL_DAL.Repositories
{
    /// <summary>
    /// IRepository implementation for MSSQL Server.
    /// </summary>
    /// <typeparam name="ItemType">Type of item that this repository has access to.</typeparam>
    public abstract class MSSQL_Repository<ItemType> : IRepository<ItemType> where ItemType : class
    {
        protected readonly IDbSet<ItemType> dbset;

        internal DataContext Context
        {
            get { return ((MSSQL_Database)Database).Context; }
        }

        internal LuceneSearch Search
        {
            get { return ((MSSQL_Database) Database).Search; }
        }

        internal virtual Document GetDocument(ItemType item)
        {
            return null;
        }

        /// <summary>
        /// Database that is used to connect to server.
        /// </summary>
        public IDatabase Database { get; set; }

        /// <summary>
        /// Constuctor.
        /// </summary>
        /// <param name="database">Database that will be used to connect to server.</param>
        protected MSSQL_Repository(MSSQL_Database database)
        {
            Database = database;
            dbset = Context.Set<ItemType>();
        }

        /// <summary>
        /// Add item to database.
        /// </summary>
        /// <param name="item">Item to be added.</param>
        public void Add(ItemType item)
        {
            CreateIndex(item);
            dbset.Add(item);
            Context.SaveChanges();
        }

        /// <summary>
        /// Add or update item in database.
        /// </summary>
        /// <param name="item">Item to be added or updated.</param>
        public void AddOrUpdate(ItemType item)
        {
            CreateIndex(item);
            dbset.AddOrUpdate(item);
            Context.SaveChanges();
        }

        /// <summary>
        /// Remove item from database.
        /// </summary>
        /// <param name="item">Item to be removed.</param>
        public void Remove(ItemType item)
        {
            RemoveIndex(item);
            dbset.Remove(item);
            Context.SaveChanges();
        }

        /// <summary>
        /// Find item by it's unique identifier.
        /// </summary>
        /// <param name="id">Unique identifier of item to be found.</param>
        /// <returns>Item with id or null.</returns>
        public ItemType Find(Guid id)
        {
            return dbset.Find(id);
        }

        /// <summary>
        /// Select item from sequence based on selector.
        /// </summary>
        /// <param name="selector">Function to transform each element.</param>
        /// <returns>List of selected items from database.</returns>
        public IEnumerable<TResult> Select<TResult>(Func<ItemType, TResult> selector)
        {
            return dbset.Select(selector);
        }

        /// <summary>
        /// Get all items from database.
        /// </summary>
        /// <returns>List of items.</returns>
        public IEnumerable<ItemType> All()
        {
            return dbset;
        }

        /// <summary>
        /// Filters sequence based on selector.
        /// </summary>
        /// <param name="selector">Function to test each element for condition.</param>
        /// <returns>List of filtered items from database.</returns>
        public IEnumerable<ItemType> Where(Func<ItemType, bool> selector)
        {
            return dbset.Where(selector);
        }

        /// <summary>
        /// Get count of elements in database.
        /// </summary>
        /// <returns>Count of elements.</returns>
        public int Count()
        {
            return dbset.Count();
        }

        /// <summary>
        /// Get count of elements filtered from database.
        /// </summary>
        /// <param name="selector">Function to test each element for condition.</param>
        /// <returns>Count of filtered elements in sequence.</returns>
        public int Count(Func<ItemType, bool> selector)
        {
            return dbset.Count(selector);
        }

        /// <summary>
        /// Get first element in sequence or null of it's not found.
        /// </summary>
        /// <param name="selector">Function to test each element for condition.</param>
        /// <returns>First element or null if it's not found.</returns>
        public ItemType FirstOrDefault(Func<ItemType, bool> selector)
        {
            return dbset.FirstOrDefault(selector);
        }

        private void RemoveIndex(ItemType item)
        {
            var document = GetDocument(item);
            if (document != null)
            {
                Search.RemoveFromLuceneIndex(document);
            }
        }

        private void CreateIndex(ItemType item)
        {
            var document = GetDocument(item);
            if (document != null)
            {
                Search.AddToLuceneIndex(document);
            }
        }
    }
}