using System;
using BusinessEntities;
using DAL_Interfaces.Repositories.Concrete;
using Lucene.Net.Documents;

namespace MSSQL_DAL.Repositories
{
    /// <summary>
    /// IUserRepository implementation for MSSQL Server.
    /// </summary>
    public class MSSQL_UserRepository: MSSQL_Repository<User>, IUserRepository
    {
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="database">Database that will be used to connect to server.</param>
        public MSSQL_UserRepository(MSSQL_Database database) : base(database)
        {
        }

        /// <summary>
        /// Find user with login.
        /// </summary>
        /// <param name="login">Login of user to be found.</param>
        /// <returns>User with inputted login or null.</returns>
        public User FindByLogin(String login)
        {
            return FirstOrDefault(u => u.Login == login);
        }

        /// <summary>
        /// Find user with e-mail.
        /// </summary>
        /// <param name="email">E-mail of user to be found.</param>
        /// <returns>User with inputted e-mail or null.</returns>
        public User FindByEmail(String email)
        {
            return FirstOrDefault(u => u.Email == email);
        }
    }
}