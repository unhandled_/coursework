using System;
using BusinessEntities;
using DAL_Interfaces.Repositories.Concrete;
using Lucene.Net.Documents;

namespace MSSQL_DAL.Repositories
{
    /// <summary>
    /// IUserRoleRepository implementation for MSSQL Server.
    /// </summary>
    public class MSSQL_UserRoleRepository: MSSQL_Repository<Role>, IUserRoleRepository
    {
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="database">Database that will be used to connect to server.</param>
        public MSSQL_UserRoleRepository(MSSQL_Database database) : base(database)
        {
        }

        public Role FindByName(String name)
        {
            return FirstOrDefault(ur => ur.RoleName == name);
        }
    }
}