using System;
using System.Linq;
using BusinessEntities;
using DAL_Interfaces.Repositories.Concrete;
using Lucene.Net.Documents;

namespace MSSQL_DAL.Repositories
{
    /// <summary>
    /// ILanguageRepository implementation for MSSQL Server.
    /// </summary>
    public class MSSQL_LanguageRepository: MSSQL_Repository<Language>, ILanguageRepository
    {
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="database">Database that will be used to connect to server.</param>
        public MSSQL_LanguageRepository(MSSQL_Database database) : base(database)
        {
        }
        
        /// <summary>
        /// Find language by it's locale name.
        /// </summary>
        /// <param name="localeName">Locale of language to be found.</param>
        /// <returns>Language with this locale name or null.</returns>
        public Language Find(String localeName)
        {
            return dbset.FirstOrDefault(l => l.Locale == localeName);
        }
    }
}