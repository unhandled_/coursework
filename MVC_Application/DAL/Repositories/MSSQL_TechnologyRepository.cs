using BusinessEntities;
using DAL_Interfaces.Repositories.Concrete;
using Lucene.Net.Documents;

namespace MSSQL_DAL.Repositories
{
    /// <summary>
    /// ITechnologyRepository implementation for MSSQL Server.
    /// </summary>
    public class MSSQL_TechnologyRepository: MSSQL_Repository<Technology>, ITechnologyRepository
    {
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="database">Database that will be used to connect to server.</param>
        public MSSQL_TechnologyRepository(MSSQL_Database database)
            : base(database)
        {
        }
    }
}