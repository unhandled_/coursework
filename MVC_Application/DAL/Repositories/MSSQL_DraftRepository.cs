using BusinessEntities;
using DAL_Interfaces.Repositories.Concrete;
using Lucene.Net.Documents;

namespace MSSQL_DAL.Repositories
{
    /// <summary>
    /// IDraftRepository implementation for MSSQL Server.
    /// </summary>
    public class MSSQL_DraftRepository: MSSQL_Repository<Draft>, IDraftRepository
    {
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="database">Database that will be used to connect to server.</param>
        public MSSQL_DraftRepository(MSSQL_Database database) : base(database)
        {
        }

        internal override Document GetDocument(Draft item)
        {
            Document result = new Document();
            result.Add(new Field("Id", item.Id.ToString(), Field.Store.YES, Field.Index.NOT_ANALYZED));
            return result;
        }
    }
}