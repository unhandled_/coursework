using BusinessEntities;
using DAL_Interfaces.Repositories.Concrete;
using Lucene.Net.Documents;

namespace MSSQL_DAL.Repositories
{
    /// <summary>
    /// ILikeRepository implementation for MSSQL Server.
    /// </summary>
    public class MSSQL_LikeRepository: MSSQL_Repository<Like>, ILikeRepository
    {
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="database">Database that will be used to connect to server.</param>
        public MSSQL_LikeRepository(MSSQL_Database database) : base(database)
        {
        }
    }
}