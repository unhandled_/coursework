﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using BusinessEntities;
using DAL_Interfaces;
using DAL_Interfaces.Repositories.Basic;
using DAL_Interfaces.Repositories.Concrete;
using Lucene.Net.Documents;
using MSSQL_DAL.Repositories;
using Ninject;

namespace MSSQL_DAL
{
    /// <summary>
    /// Implementation of IDatabase interface for work with MSSQL Server
    /// </summary>
    public class MSSQL_Database: IDatabase
    {
        private static object locker = new object();

        internal DataContext Context
        {
            get
            {
                lock (locker)
                {
                    string ocKey = "ocm_" + HttpContext.Current.GetHashCode().ToString("x");
                    if (!HttpContext.Current.Items.Contains(ocKey))
                    {
                        var newContext = new DataContext(ConnectionString);
                        if (!newContext.Database.Exists())
                        {
                            newContext.Database.Initialize(false);
                        }
                        HttpContext.Current.Items.Add(ocKey, newContext);
                    }
                    return HttpContext.Current.Items[ocKey] as DataContext;
                }
                //return context ?? (context = new DataContext(ConnectionString));
            }
        }

        internal LuceneSearch Search { get; set; }

        internal String ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["MSSQL_Database"].ConnectionString;
            }
        }

        /// <summary>
        /// Repository for access to answers.
        /// </summary>
        public IAnswerRepository AnswerRepository { get; set; }

        /// <summary>
        /// Repository for access to users.
        /// </summary>
        public IUserRepository UserRepository { get; set; }

        /// <summary>
        /// Repository for access to user roles.
        /// </summary>
        public IUserRoleRepository UserRoleRepository { get; set; }

        /// <summary>
        /// Repository for access to tags.
        /// </summary>
        public ITagRepository TagRepository { get; set; }

        /// <summary>
        /// Repository for access to tasks.
        /// </summary>
        public ITaskRepository TaskRepository { get; set; }

        /// <summary>
        /// Repository for access to technologies.
        /// </summary>
        public ITechnologyRepository TechnologyRepository { get; set; }

        /// <summary>
        /// Repository for access to drafts.
        /// </summary>
        public IDraftRepository DraftRepository { get; set; }

        /// <summary>
        /// Repository for access to likes.
        /// </summary>
        public ILikeRepository LikeRepository { get; set; }

        /// <summary>
        /// Repository for access to languages.
        /// </summary>
        public ILanguageRepository LanguageRepository { get; set; }
        
        /// <summary>
        /// Commit changes to database.
        /// </summary>
        public void SaveChanges()
        {
            Context.SaveChanges();
        }

        /// <summary>
        /// Implementation of IDisposable interface.
        /// </summary>
        public void Dispose()
        {
            
        }

        /// <summary>
        /// Default parameterless constructor.
        /// </summary>
        public MSSQL_Database()
        {
            InitializeRepositories();
            Search = new LuceneSearch();
        }

        /// <summary>
        /// Get instance of repository working with specified items type.
        /// </summary>
        /// <typeparam name="ItemType">The type of the tem type.</typeparam>
        /// <returns>Requested repository or null if item type if not supported.</returns>
        public IRepository<ItemType> Repository<ItemType>()
        {
            if (typeof (ItemType) == typeof (User))
            {
                return UserRepository as IRepository<ItemType>;
            }
            if (typeof (ItemType) == typeof (Tag))
            {
                return TagRepository as IRepository<ItemType>;
            }
            if (typeof (ItemType) == typeof (Task))
            {
                return TaskRepository as IRepository<ItemType>;
            }
            if (typeof (ItemType) == typeof (Technology))
            {
                return TechnologyRepository as IRepository<ItemType>;
            }
            if (typeof (ItemType) == typeof (Draft))
            {
                return DraftRepository as IRepository<ItemType>;
            }
            if (typeof (ItemType) == typeof (Language))
            {
                return LanguageRepository as IRepository<ItemType>;
            }
            if (typeof (ItemType) == typeof (Like))
            {
                return LikeRepository as IRepository<ItemType>;
            }
            if (typeof (ItemType) == typeof (Answer))
            {
                return AnswerRepository as IRepository<ItemType>;
            }
            return typeof (ItemType) == typeof (Role) ? UserRepository as IRepository<ItemType> : null;
        }

        private void InitializeRepositories()
        {
            UserRepository = new MSSQL_UserRepository(this);
            TagRepository = new MSSQL_TagRepository(this);
            TaskRepository = new MSSQL_TaskRepository(this);
            TechnologyRepository = new MSSQL_TechnologyRepository(this);
            DraftRepository = new MSSQL_DraftRepository(this);
            LanguageRepository = new MSSQL_LanguageRepository(this);
            LikeRepository = new MSSQL_LikeRepository(this);
            AnswerRepository = new MSSQL_AnswerRepository(this);
            UserRoleRepository = new MSSQL_UserRoleRepository(this);
        }
    
        public void RecreateTaskSearchIndex()
        {
            MSSQL_TaskRepository taskRep = ((MSSQL_TaskRepository) TaskRepository);
            IEnumerable<Document> documents = taskRep.All().Select(taskRep.GetDocument).ToList();
            Search.AddToLuceneIndex(documents);
        }
    }
}
