using System;
using System.Web.Security;
using BL_Interfaces.AccessComponents.Concrete;
using BusinessEntities;
using DAL_Interfaces;

namespace BL_Interfaces
{
    /// <summary>
    /// Interface giving access to operations with application logics.
    /// </summary>
    public interface ILogics: IDisposable
    {
        /// <summary>
        /// Currently logged in user.
        /// </summary>
        User CurrentUser { get; set; }

        /// <summary>
        /// Database that is used in current logics.
        /// </summary>
        IDatabase Database { get; set; }

        /// <summary>
        /// Answer access component.
        /// </summary>
        IAnswerAccessComponent AnswerAC { get; set; }

        /// <summary>
        /// Draft access component.
        /// </summary>
        IDraftAccessComponent DraftAC { get; set; }
        
        /// <summary>
        /// Language access component.
        /// </summary>
        ILanguageAccessComponent LanguageAC { get; set; }

        /// <summary>
        /// Tag access component.
        /// </summary>
        ITagAccessComponent TagAC { get; set; }

        /// <summary>
        /// Task access component.
        /// </summary>
        ITaskAccessComponent TaskAC { get; set; }

        /// <summary>
        /// Technology access component.
        /// </summary>
        ITechnologyAccessComponent TechnologyAC { get; set; }

        /// <summary>
        /// User access component.
        /// </summary>
        IUserAccessComponent UserAC { get; set; }
    }
}