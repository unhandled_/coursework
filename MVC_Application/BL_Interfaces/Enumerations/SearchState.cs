﻿namespace BL_Interfaces.Enumerations
{
    public enum SearchState
    {
        ItemFound,
        ItemNotFound,
    }
}