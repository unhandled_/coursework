using System;

namespace BL_Interfaces.Enumerations
{
    [Flags]
    public enum OperationAccess
    {
        Authorized = 0x1,
        Owner = 0x2 | Authorized,
        AlternativeRole = 0x4 | Authorized,
    }
}