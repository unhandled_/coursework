using System;

namespace BL_Interfaces.Exceptions
{
    /// <summary>
    /// Exception is thrown when somebody tries to create user role, that already exists in database.
    /// </summary>
    public class UserRoleAlreadyExistsException : Exception
    {
        /// <summary>
        /// Default parameterless constructor.
        /// </summary>
        public UserRoleAlreadyExistsException()
            : base(Properties.ExceptionStrings.UserRoleAlreadyExistsString)
        {

        }
    }
}