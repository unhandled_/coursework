using System;

namespace BL_Interfaces.Exceptions
{
    /// <summary>
    /// Exception is thrown when user with inputted mail was not found in database.
    /// </summary>
    public class EmailNotFoundException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EmailNotFoundException" /> class.
        /// </summary>
        public EmailNotFoundException()
            : base(Properties.ExceptionStrings.EmailNotFoundException)
        {
        }
    }
}