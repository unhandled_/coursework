﻿using System;
using BL_Interfaces.Properties;

namespace BL_Interfaces.Exceptions
{
    /// <summary>
    /// Exception is thrown when object that have dependencies in database should be removed.
    /// </summary>
    public class ExistingDependencyException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ExistingDependencyException" /> class.
        /// </summary>
        public ExistingDependencyException()
            : base(ExceptionStrings.DependencyString)
        {
        }
    }
}