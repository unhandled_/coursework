﻿using System;

namespace BL_Interfaces.Exceptions
{
    public class EmailAlreadyExistsException : Exception
    {
        public EmailAlreadyExistsException()
            : base(Properties.ExceptionStrings.EmailAlreadyExistsString)
        {
        }
    }
}