using System;

namespace BL_Interfaces.Exceptions
{
    /// <summary>
    /// Exception is thrown when user inputted wrong login or password while trying to log in.
    /// </summary>
    public class AuthorizationFailedExpcetion : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AuthorizationFailedExpcetion" /> class.
        /// </summary>
        public AuthorizationFailedExpcetion()
            : base(Properties.ExceptionStrings.AuthentificationFailedString)
        {
        }
    }
}