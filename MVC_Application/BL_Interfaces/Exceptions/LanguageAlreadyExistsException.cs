using System;
using BL_Interfaces.Properties;

namespace BL_Interfaces.Exceptions
{
    /// <summary>
    /// Exception is thrown when somebody tries to create language, that already exists in database.
    /// </summary>
    public class LanguageAlreadyExistsException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LanguageAlreadyExistsException" /> class.
        /// </summary>
        public LanguageAlreadyExistsException()
            : base(ExceptionStrings.LanguageAlreadyExistsString)
        {

        }
    }
}