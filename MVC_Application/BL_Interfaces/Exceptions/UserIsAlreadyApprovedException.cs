using System;
using BL_Interfaces.Properties;

namespace BL_Interfaces.Exceptions
{
    /// <summary>
    /// Exception that is thrown when somebody tries to do verification for already approved user.
    /// </summary>
    public class UserIsAlreadyApprovedException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UserIsAlreadyApprovedException" /> class.
        /// </summary>
        public UserIsAlreadyApprovedException()
            : base(ExceptionStrings.UserIsAlreadyApproved)
        {
        }
    }
}