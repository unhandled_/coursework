using System;
using BL_Interfaces.Properties;

namespace BL_Interfaces.Exceptions
{
    /// <summary>
    /// Exception is thrown when sending e-mail verification failed with some reason.
    /// </summary>
    public class VerificationSendException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="VerificationSendException" /> class.
        /// </summary>
        /// <param name="exception">The exception.</param>
        public VerificationSendException(Exception exception)
            : base(ExceptionStrings.VerificationSendString, exception)
        {
        }
    }


    /// <summary>
    /// Thrown if requested answer was not found in database.
    /// </summary>
    public class AnswerNotFoundException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AnswerNotFoundException" /> class.
        /// </summary>
        public AnswerNotFoundException()
            : base(ExceptionStrings.AnswerNotFoundString)
        {
        }
    }

    /// <summary>
    /// Thrown if requested tag was not found in database.
    /// </summary>
    public class TagNotFoundException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TagNotFoundException" /> class.
        /// </summary>
        public TagNotFoundException()
            : base(ExceptionStrings.TagNotFoundString)
        {
        }
    }

    /// <summary>
    /// Exception is thrown when registration failed with some strange reason.
    /// </summary>
    public class UnhandledRegistrationException : Exception
    {
        /// <summary>
        /// Gets or sets the inner message.
        /// </summary>
        /// <value>
        /// The inner message.
        /// </value>
        public String InnerMessage { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="UnhandledRegistrationException" /> class.
        /// </summary>
        /// <param name="innerMessage">The inner message.</param>
        public UnhandledRegistrationException(String innerMessage)
            : base(Properties.ExceptionStrings.UnhandledRegistrationString)
        {
            InnerMessage = innerMessage;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class EmailAlreadyExistsException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EmailAlreadyExistsException" /> class.
        /// </summary>
        public EmailAlreadyExistsException()
            : base(ExceptionStrings.EmailAlreadyExistsString)
        {
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class LoginAlreadyExistsException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LoginAlreadyExistsException" /> class.
        /// </summary>
        public LoginAlreadyExistsException()
            : base(ExceptionStrings.LoginAlreadyExistsString)
        {
        }
    }



    /// <summary>
    /// Thrown if user inputs wrong password while performing operation of changing of the password
    /// </summary>
    public class IncorrectPasswordException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LoginAlreadyExistsException" /> class.
        /// </summary>
        public IncorrectPasswordException()
            : base(ExceptionStrings.IncorrectPasswordString)
        {
        }
    }
}