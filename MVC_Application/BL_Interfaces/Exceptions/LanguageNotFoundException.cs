using System;
using BL_Interfaces.Properties;

namespace BL_Interfaces.Exceptions
{
    /// <summary>
    /// Exception is thrown when somebody tries to find language, that doesn't exist in database.
    /// </summary>
    public class LanguageNotFoundException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LanguageNotFoundException" /> class.
        /// </summary>
        public LanguageNotFoundException()
            : base(ExceptionStrings.LanguageNotFoundString)
        {

        }
    }
}