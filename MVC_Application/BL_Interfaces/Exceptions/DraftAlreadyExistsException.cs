using System;
using BL_Interfaces.Properties;

namespace BL_Interfaces.Exceptions
{
    /// <summary>
    /// Exception that is thrown when user tries to create draft of task that already has a draft.
    /// </summary>
    public class DraftAlreadyExistsException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DraftAlreadyExistsException" /> class.
        /// </summary>
        public DraftAlreadyExistsException()
            : base(ExceptionStrings.DraftAlreadyExistsString)
        {
        }
    }
}