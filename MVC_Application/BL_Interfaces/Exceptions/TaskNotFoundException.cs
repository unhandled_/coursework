using System;

namespace BL_Interfaces.Exceptions
{
    /// <summary>
    /// Exception is thrown when somebody tries to find task, that doesn't exist in database.
    /// </summary>
    public class TaskNotFoundException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TaskNotFoundException" /> class.
        /// </summary>
        public TaskNotFoundException()
            : base(Properties.ExceptionStrings.TaskNotFoundString)
        {

        }
    }
}