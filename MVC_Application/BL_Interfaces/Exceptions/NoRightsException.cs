using System;
using BL_Interfaces.Properties;

namespace BL_Interfaces.Exceptions
{
    /// <summary>
    /// Exception is thrown when user tries to perform some action that he 
    /// </summary>
    public class NoRightsException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NoRightsException" /> class.
        /// </summary>
        public NoRightsException()
            : base(ExceptionStrings.NoRightsString)
        {
        }
    }
}