using System;

namespace BL_Interfaces.Exceptions
{
    /// <summary>
    /// Exception is thrown when somebody tries to user role, that doesn't exist in database.
    /// </summary>
    public class UserRoleNotFoundException : Exception
    {
        /// <summary>
        /// Default parameterless constructor.
        /// </summary>
        public UserRoleNotFoundException()
            : base(Properties.ExceptionStrings.UserRoleNotFoundString)
        {

        }
    }
}