﻿using System;
using BL_Interfaces.Properties;

namespace BL_Interfaces.Exceptions
{
    /// <summary>
    /// Exception is thrown when database doesn't work properly.
    /// </summary>
    public class DatabaseFailedException: Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DatabaseFailedException" /> class.
        /// </summary>
        /// <param name="innerException">The inner exception.</param>
        public DatabaseFailedException(Exception innerException)
            : base(ExceptionStrings.DatabaseFailString, innerException)
        {
        }
    }
}