using System;

namespace BL_Interfaces.Exceptions
{
    /// <summary>
    /// Exception is thrown when user with inputted login was not found in database.
    /// </summary>
    public class LoginNotFoundException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LoginNotFoundException" /> class.
        /// </summary>
        public LoginNotFoundException()
            : base(Properties.ExceptionStrings.LoginNotFoundString)
        {
        }
    }
}