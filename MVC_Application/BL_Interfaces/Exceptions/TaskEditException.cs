using System;
using BL_Interfaces.Properties;

namespace BL_Interfaces.Exceptions
{
    /// <summary>
    /// Exception is thrown when somebody tries to remove task that have more that zero solved users.
    /// </summary>
    public class TaskEditException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TaskEditException" /> class.
        /// </summary>
        public TaskEditException()
            : base(ExceptionStrings.TaskEditString)
        {

        }
    }
}