using System;
using BL_Interfaces.Properties;

namespace BL_Interfaces.Exceptions
{
    /// <summary>
    /// Exception is thrown when somebody tries to find draft, that doesn't exist in database.
    /// </summary>
    public class DraftNotFoundException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DraftNotFoundException" /> class.
        /// </summary>
        public DraftNotFoundException()
            : base(ExceptionStrings.DraftNotFoundString)
        {

        }
    }
}