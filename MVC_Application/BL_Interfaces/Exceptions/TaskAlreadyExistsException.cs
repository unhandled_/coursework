using System;

namespace BL_Interfaces.Exceptions
{
    /// <summary>
    /// Exception is thrown when somebody tries to create task, that already exists in database.
    /// </summary>
    public class TaskAlreadyExistsException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TaskAlreadyExistsException" /> class.
        /// </summary>
        public TaskAlreadyExistsException()
            : base(Properties.ExceptionStrings.TaskAlreadyExistsString)
        {

        }
    }
}