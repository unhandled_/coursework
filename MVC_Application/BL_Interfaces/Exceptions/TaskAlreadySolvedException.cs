using System;

namespace BL_Interfaces.Exceptions
{
    /// <summary>
    /// Exception is thrown when user that already solved current task tries to solve it again.
    /// </summary>
    public class TaskAlreadySolvedException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TaskAlreadySolvedException" /> class.
        /// </summary>
        public TaskAlreadySolvedException() : base(Properties.ExceptionStrings.TaskAlreadySolvedString)
        {
        }
    }
}