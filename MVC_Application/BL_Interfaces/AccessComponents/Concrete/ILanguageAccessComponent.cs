using System;
using BL_Interfaces.AccessComponents.Basic;
using BL_Interfaces.Exceptions;
using BusinessEntities;

namespace BL_Interfaces.AccessComponents.Concrete
{
    /// <summary>
    /// Interface giving functionality for access to operations with languages.
    /// </summary>
    public interface ILanguageAccessComponent : IAccessComponent<Language>
    {
        /// <summary>
        /// Create language in database.
        /// </summary>
        /// <param name="locale">Locale name of this language.</param>
        /// <param name="name">Name of this language.</param>
        /// <returns>Created language.</returns>
        /// <exception cref="DatabaseFailedException">Thrown if there are errors in database.</exception>
        /// <exception cref="LanguageAlreadyExistsException">Thown if locale name already exists in database.</exception>
        Language Create(String locale, String name);

        /// <summary>
        /// Edit language.
        /// </summary>
        /// <param name="languageId">Language's unique identifier.</param>
        /// <param name="newLocale">New locale name of language.</param>
        /// <param name="newName">New name of language.</param>
        /// <exception cref="LanguageAlreadyExistsException">Thrown if locale name already exists in database.</exception>
        /// <exception cref="DatabaseFailedException">Thrown if there are errors in database.</exception>
        /// <exception cref="LanguageNotFoundException">Thrown if language was not found in database.</exception>
        void Edit(Guid languageId, String newLocale, String newName);

        /// <summary>
        /// Remove language with some unique identifier.
        /// </summary>
        /// <param name="locale">Locale name of language to be removed.</param>
        /// <exception cref="ExistingDependencyException">Exception is thrown when language to be removed has existing dependencies in database.</exception>
        /// <exception cref="DatabaseFailedException">Thrown if there are errors in database.</exception>
        /// <exception cref="LanguageNotFoundException">Thrown if language was not found in database.</exception>
        /// <exception cref="NoRightsException">Thrown if user have no rights to perform current action.</exception>
        void Remove(String locale);

        /// <summary>
        /// Find language with some locale name.
        /// </summary>
        /// <param name="locale">Locale name of language to be found.</param>
        /// <param name="throwExceptionIfNotFound">If true and language was not found in database - LanguageNotFoundException is thrown.</param>
        /// <exception cref="DatabaseFailedException">Thrown if there are errors in database.</exception>
        /// <exception cref="LanguageNotFoundException">Thrown if language was not found in database.</exception>
        Language Find(String locale, bool throwExceptionIfNotFound);
    }
}