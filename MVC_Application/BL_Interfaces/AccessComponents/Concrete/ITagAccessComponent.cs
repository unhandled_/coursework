using System;
using System.Collections.Generic;
using BL_Interfaces.AccessComponents.Basic;
using BL_Interfaces.Exceptions;
using BusinessEntities;

namespace BL_Interfaces.AccessComponents.Concrete
{
    /// <summary>
    /// Interface giving functionality for access to operations with tags.
    /// </summary>
    public interface ITagAccessComponent : IAccessComponent<Tag>
    {
        /// <summary>
        /// Cleans up (Removes all unused tags from database).
        /// </summary>
        /// <exception cref="DatabaseFailedException">Thrown if there are errors in database.</exception>
        void CleanUp();

        /// <summary>
        /// Get tag from database. If no tag in database with inputted name was found - new tag is created.
        /// </summary>
        /// <param name="name">Name of tag.</param>
        /// <returns>Tag from database or newly created.</returns>
        /// <exception cref="DatabaseFailedException">Thrown if there are errors in database.</exception>
        Tag GetWithName(String name);

        /// <summary>
        /// Find tag with by it's text.
        /// </summary>
        /// <param name="text">Text of tag to be found.</param>
        /// <returns>Tag or null.</returns>
        /// <exception cref="DatabaseFailedException">Thrown if there are errors in database.</exception>
        Tag Find(String text);

        /// <summary>
        /// Get list of tags that have text field containing inputted subString.
        /// </summary>
        /// <param name="subString">SubString used to get tags.</param>
        /// <returns>List of tags.</returns>
        /// <exception cref="DatabaseFailedException">Thrown if there are errors in database.</exception>
        IEnumerable<Tag> FindTagsContainingText(String subString);

        /// <summary>
        /// Get list of most popular tags.
        /// </summary>
        /// <param name="tagsCount">Number of tags to be selected.</param>
        /// <param name="languageLocale">Language of tags.</param>
        /// <returns>List of most popular tags.</returns>
        /// <exception cref="DatabaseFailedException">Thrown if there are errors in database.</exception>
        IEnumerable<Tag> GetMostPopular(Int32 tagsCount, String languageLocale);
    }
}