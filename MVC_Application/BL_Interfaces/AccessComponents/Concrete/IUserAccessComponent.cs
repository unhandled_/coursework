using System;
using BL_Interfaces.AccessComponents.Basic;
using BL_Interfaces.Exceptions;
using BusinessEntities;

namespace BL_Interfaces.AccessComponents.Concrete
{
    /// <summary>yj 
    /// Interface giving functionality for access to operations with users.
    /// </summary>
    public interface IUserAccessComponent: IAccessComponent<User>
    {
        void Edit(Guid id, String newLogin, String newPassword, String newEmail);

        void Edit(String login, String oldPassword, String newPassword, String newMail);

        /// <summary>
        /// Registers user.
        /// </summary>
        /// <param name="login">The login.</param>
        /// <param name="password">The password.</param>
        /// <param name="email">The email.</param>
        /// <param name="isApproved">if set to <c>true</c> confirmation will be send of specified e-mail.</param>
        /// <exception cref="DatabaseFailedException">Thrown if there are errors in database.</exception>
        /// <exception cref="LoginAlreadyExistsException">Thrown if specified login already exists in database.</exception>
        /// <exception cref="EmailAlreadyExistsException">Thrown if specified e-mail already exists in database.</exception>
        void Register(String login, String password, String email, bool isApproved);

        /// <summary>
        /// Finds user by mail.
        /// </summary>
        /// <param name="email">E-mail of user to be found.</param>
        /// <param name="throwExceptionIfNotFound">if set to <c>true</c> [throw exception if not found].</param>
        /// <returns>
        /// User or null (if exception is not thrown).
        /// </returns>
        /// <exception cref="DatabaseFailedException">Thrown if there are errors in database.</exception>
        /// <exception cref="EmailNotFoundException">Thrown if user with specified e-mail was not found in database.</exception>
        User FindByMail(String email, bool throwExceptionIfNotFound);

        /// <summary>
        /// Finds user by login.
        /// </summary>
        /// <param name="login">The login.</param>
        /// <param name="throwExceptionIfNotFound">if set to <c>true</c> [throw exception if not found].</param>
        /// <returns>User. </returns>
        /// <exception cref="DatabaseFailedException">Thrown if there are errors in database.</exception>
        /// <exception cref="LoginNotFoundException">Exception is thrown when user with inputted login was not found in database.</exception>
        User FindByLogin(String login, bool throwExceptionIfNotFound);

        /// <summary>
        /// Sends the verification.
        /// </summary>
        /// <param name="login">Login of user to be verified.</param>
        /// <exception cref="DatabaseFailedException">Thrown if there are errors in database.</exception>
        /// <exception cref="LoginNotFoundException">Exception is thrown when user with inputted login was not found in database.</exception>
        /// <exception cref="VerificationSendException">Thrown if errors occured while sending letter with verification.</exception>
        void SendVerification(String login);

        /// <summary>
        /// Approves user with specified login.
        /// </summary>
        /// <param name="login">Login of user to be approved.</param>
        /// <param name="confirmationToken">The confirmation token.</param>
        /// <returns>True if user was successfully approved.</returns>
        /// <exception cref="DatabaseFailedException">Thrown if there are errors in database.</exception>
        /// <exception cref="LoginNotFoundException">Exception is thrown when user with inputted login was not found in database.</exception>
        bool Approve(String login, String confirmationToken);

        /// <summary>
        /// Sets the language of specified user.
        /// </summary>
        /// <param name="login">Login of user that changes his language.</param>
        /// <param name="languageLocale">Language locale.</param>
        /// <exception cref="DatabaseFailedException">Thrown if there are errors in database.</exception>
        /// <exception cref="LoginNotFoundException">Exception is thrown when user with inputted login was not found in database.</exception>
        /// <exception cref="LanguageNotFoundException">Thrown if language was not found in database.</exception>
        void SetLanguage(String login, String languageLocale);

        /// <summary>
        /// Removes user with specified login (removing user must be currently logged in).
        /// </summary>
        /// <param name="login">Login of user to be removed.</param>
        /// <exception cref="NoRightsException">Exception is thrown when user have no rights to perform some action.</exception>
        /// <exception cref="DatabaseFailedException">Thrown if there are errors in database.</exception>
        /// <exception cref="LoginNotFoundException">Exception is thrown when user with inputted login was not found in database.</exception>
        void RemoveByUser(String login);

        /// <summary>
        /// Recovers user with specified login.
        /// </summary>
        /// <param name="login">The login.</param>
        /// <param name="password">The password.</param>
        void RecoverByUser(String login, string password);

        /// <summary>
        /// Removes or recovers user by admin. (currently logged in user must have administrator's rights)
        /// </summary>
        /// <param name="login">The login.</param>
        void RemoveOrRecoverByAdmin(String login);

        /// <summary>
        /// Logs user out.
        /// </summary>
        /// <exception cref="DatabaseFailedException">Thrown if there are errors in database.</exception>
        void LogOut();

        /// <summary>
        /// Logs user in.
        /// </summary>
        /// <param name="login">Login of user to be logged in.</param>
        /// <param name="password">Password of user to be logged in.</param>
        /// <exception cref="AuthorizationFailedExpcetion">Exception is thrown when password or login are incorrect.</exception>
        /// <exception cref="DatabaseFailedException">Thrown if there are errors in database.</exception>
        /// <exception cref="LoginNotFoundException">Exception is thrown when user with inputted login was not found in database.</exception>
        void LogIn(String login, String password);
    }
}