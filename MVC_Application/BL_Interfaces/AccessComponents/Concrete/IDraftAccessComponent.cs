using System;
using System.Collections.Generic;
using BL_Interfaces.AccessComponents.Basic;
using BL_Interfaces.Exceptions;
using BusinessEntities;

namespace BL_Interfaces.AccessComponents.Concrete
{
    /// <summary>
    /// Interface giving functionality for access to operations with drafts.
    /// </summary>
    public interface IDraftAccessComponent : IAccessComponent<Draft>
    {
        void RemoveTag(Guid draftId, String tagText);

        void RemoveAnswer(Guid draftId, String answerText);

        /// <summary>
        /// Create draft in database from original task.
        /// </summary>
        /// <param name="originalId">Unique identifier of original task.</param>
        /// <returns>Created draft.</returns> 
        /// <exception cref="DatabaseFailedException">Thrown if there are errors in database.</exception>
        /// <exception cref="TaskNotFoundException">Thrown if original was not found in database.</exception>
        Draft Create(Guid originalId);

        /// <summary>
        /// Create new draft in database.
        /// </summary>
        /// <param name="description">Description of draft.</param>
        /// <param name="text">Text of draft.</param>
        /// <param name="name">Name of draft.</param>
        /// <param name="technology">Technology that is used in this draft.</param>
        /// <param name="locale">Language of draft. </param>
        /// <param name="tags">List of tags for this draft.</param>
        /// <param name="answers">List of answers for this draft.</param>
        /// <returns>Created draft.</returns>
        /// <exception cref="DatabaseFailedException">Thrown if there are errors in database.</exception>
        /// <exception cref="LanguageNotFoundException">Thrown if there is not language in database with inputted locale.</exception>
        Draft Create(String description, String text, String name, String technology, String locale,
            List<String> tags, List<String> answers);

        /// <summary>
        /// Edit draft.
        /// </summary>
        /// <param name="draftId">Draft's to be edited unique identifier.</param>
        /// <param name="newDescription">New draft's description.</param>
        /// <param name="newText">New draft's text.</param>
        /// <param name="newName">New draft's name.</param>
        /// <param name="technology">New draft's technology.</param>
        /// <param name="locale">New language of draft. </param>
        /// <param name="newTags">List of new tags.</param>
        /// <param name="newAnswers">List of new answers.</param>
        /// <exception cref="DraftNotFoundException">Thrown if draft was not found in database.</exception>
        /// <exception cref="DatabaseFailedException">Thrown if there are errors in database.</exception>
        /// <exception cref="LanguageNotFoundException">Thrown if there is not language in database with inputted locale.</exception>
        void Edit(Guid draftId, String newDescription, String newText, String newName, String technology,
            String locale, List<String> newTags, List<String> newAnswers);

        /// <summary>
        /// Save draft as task. If no original - new task created, otherwise original will be rewritten.
        /// </summary>
        /// <param name="draftId">Draft's unique identifier.</param>
        /// <exception cref="DraftNotFoundException">Thrown if draft was not found in database.</exception>
        /// <exception cref="DatabaseFailedException">Thrown if there are errors in database.</exception>
        void SaveChanges(Guid draftId);

        /// <summary>
        /// Remove draft from database.
        /// </summary>
        /// <param name="draftId">Draft's unique identifier.</param>
        /// <exception cref="DraftNotFoundException">Thrown if draft was not found in database.</exception>
        /// <exception cref="DatabaseFailedException">Thrown if there are errors in database.</exception>
        void Remove(Guid draftId);
    }
}