using System;
using System.Collections.Generic;
using BL_Interfaces.AccessComponents.Basic;
using BL_Interfaces.Exceptions;
using BusinessEntities;

namespace BL_Interfaces.AccessComponents.Concrete
{
    /// <summary>
    /// Interface giving functionality for access to operations with technologies.
    /// </summary>
    public interface ITechnologyAccessComponent: IAccessComponent<Technology>
    {
        /// <summary>
        /// Cleans up (Removes all unused technologies from database).
        /// </summary>
        /// <exception cref="DatabaseFailedException">Thrown if there are errors in database.</exception>
        void CleanUp();

        /// <summary>
        /// Get list of technologies that have name field containing inputted subString.
        /// </summary>
        /// <param name="subString">SubString used to get technologies.</param>
        /// <returns>List of technologies.</returns>
        /// <exception cref="DatabaseFailedException">Thrown if there are errors in database.</exception>
        IEnumerable<Technology> FindTechnologiesContainingText(String subString);

        /// <summary>
        /// Get technology from database. If no technology was not found - new technology will be created.
        /// </summary>
        /// <param name="name">Name of technology.</param>
        /// <returns>Technology with inputted name or newly created.</returns>
        /// <exception cref="DatabaseFailedException">Thrown if there are errors in database.</exception>
        Technology GetWithName(String name);
    }
}