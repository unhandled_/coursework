using System;
using System.Collections.Generic;
using BL_Interfaces.AccessComponents.Basic;
using BL_Interfaces.Exceptions;
using BusinessEntities;

namespace BL_Interfaces.AccessComponents.Concrete
{
    /// <summary>
    /// Interface giving functionality for access to operations with tasks.
    /// </summary>
    public interface ITaskAccessComponent: IAccessComponent<Task>
    {
        /// <summary>
        /// Finds tasks by specified query.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <returns>Sequence of tasks.</returns>
        IEnumerable<Task> Find(String query);
        
        /// <summary>
        /// Create new task in database.
        /// </summary>
        /// <param name="description">Description of task.</param>
        /// <param name="text">Text of task.</param>
        /// <param name="name">Name of task.</param>
        /// <param name="technology">Technology that is used in this task.</param>
        /// <param name="locale">Language of task.</param>
        /// <param name="tags">List of tags for this task.</param>
        /// <param name="answers">List of answers for this task.</param>
        /// <returns>Created task.</returns>
        /// <exception cref="TaskAlreadyExistsException">Thrown if task with inputted name already exists in database.</exception>
        /// <exception cref="DatabaseFailedException">Thrown if there are errors in database.</exception>
        /// <exception cref="LanguageNotFoundException">Thrown if there is not language in database with inputted locale.</exception>
        /// <exception cref="NoRightsException">Thrown if user have no rights to perform current action.</exception>
        Task Create(String description, String text, String name, String technology, String locale, 
            List<String> tags, List<String> answers);

        /// <summary>
        /// Try to solve task by current user.
        /// </summary>
        /// <param name="taskId">Unique identifier of task to be solved.</param>
        /// <param name="answer">Text of estimated answer.</param>
        /// <returns>True if task was solved successfully, otherwise false.</returns>
        /// <exception cref="TaskNotFoundException">Thrown if task was not found in database.</exception>
        /// <exception cref="DatabaseFailedException">Thrown if task are errors in database.</exception>
        /// <exception cref="TaskAlreadySolvedException">Thrown if task is already solved by current user.</exception>
        /// <exception cref="NoRightsException">Thrown if user have no rights to perform current action.</exception>
        Boolean TrySolve(Guid taskId, String answer);

        /// <summary>
        /// Current user pressed Like button of task.
        /// </summary>
        /// <param name="taskId">Unique identifier of task to be liked.</param>
        /// <returns>New number of likes.</returns>
        /// <exception cref="TaskNotFoundException">Thrown if task was not found in database.</exception>
        /// <exception cref="DatabaseFailedException">Thrown if task are errors in database.</exception>
        /// <exception cref="NoRightsException">Thrown if user have no rights to perform current action.</exception>
        Int32 Like(Guid taskId);

        /// <summary>
        /// Remove task by it's unique identifier.
        /// </summary>
        /// <param name="taskId">Unique identifier of task to be removed.</param>
        /// <exception cref="TaskEditException">Thrown if task cannot be edited anymore.</exception>
        /// <exception cref="TaskNotFoundException">Thrown if task was not found in database.</exception>
        /// <exception cref="DatabaseFailedException">Thrown if task are errors in database.</exception>
        /// <exception cref="NoRightsException">Thrown if user have no rights to perform current action.</exception>
        void Remove(Guid taskId);
    }
}