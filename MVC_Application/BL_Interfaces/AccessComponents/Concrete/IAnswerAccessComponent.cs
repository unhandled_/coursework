using System;
using BL_Interfaces.AccessComponents.Basic;
using BL_Interfaces.Exceptions;
using BusinessEntities;

namespace BL_Interfaces.AccessComponents.Concrete
{
    /// <summary>
    /// Interface giving functionality for access to operations with answers.
    /// </summary>
    public interface IAnswerAccessComponent : IAccessComponent<Answer>
    {        
        /// <summary>
        /// Creates new answer in database with specified text.
        /// </summary>
        /// <param name="text">Text of answer.</param>
        /// <returns>
        /// New answer.
        /// </returns>
        /// <exception cref="DatabaseFailedException">Thrown if there are errors in database.</exception>
        Answer Create(String text);
    }

}