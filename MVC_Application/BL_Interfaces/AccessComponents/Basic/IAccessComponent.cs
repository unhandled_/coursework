﻿using System;
using System.Collections.Generic;
using BL_Interfaces.Enumerations;

namespace BL_Interfaces.AccessComponents.Basic
{
    /// <summary>
    /// Interface giving functionality for access to operations with items.
    /// </summary>
    /// <typeparam name="ItemType">Type of item that is used in access component.</typeparam>
    public interface IAccessComponent<out ItemType>
    {
        /// <summary>
        /// Gets or sets the logics.
        /// </summary>
        /// <value>
        /// The logics.
        /// </value>
        ILogics Logics { get; set; }

        /// <summary>
        /// Find item by it's unique identifier.
        /// </summary>
        /// <param name="id">Unique identifier of item.</param>
        /// <returns>Item or null if it's not found.</returns>
        ItemType Find(Guid id);

        /// <summary>
        /// Filters sequence based on selector.
        /// </summary>
        /// <param name="selector">Function to test each element for condition.</param>
        /// <returns>List of filtered items from database.</returns>
        IEnumerable<ItemType> Where(Func<ItemType, Boolean> selector);

        /// <summary>
        /// Select all elements from sequence.
        /// </summary>
        /// <returns>List of items.</returns>
        IEnumerable<ItemType> All();

        /// <summary>
        /// Get count of elements in sequence.
        /// </summary>
        /// <returns>Count of elements.</returns>
        Int32 Count();

        /// <summary>
        /// Get count of elements filtered from sequence.
        /// </summary>
        /// <param name="selector">Function to test each element for condition.</param>
        /// <returns>Count of filtered elements in sequence.</returns>
        Int32 Count(Func<ItemType, Boolean> selector);

        /// <summary>
        /// Get first element in sequence or null of it's not found.
        /// </summary>
        /// <param name="selector">Function to test each element for condition.</param>
        /// <returns>First element or null if it's not found.</returns>
        ItemType FirstOrDefault(Func<ItemType, Boolean> selector);

        /// <summary>
        /// Get list of filtered items with paging.
        /// </summary>
        /// <param name="pageNum">Number of current page.</param>
        /// <param name="itemsPerPage">Number of items for single page.</param>
        /// <param name="selector">Function to test each element for condition.</param>
        /// <returns>List of filtered and paged items.</returns>
        IEnumerable<ItemType> GetPage(Int32 pageNum, Int32 itemsPerPage, Func<ItemType, Boolean> selector);

        /// <summary>
        /// Get list of filtered items with paging and ordered by descending.
        /// </summary>
        /// <param name="pageNum">Number of current page.</param>
        /// <param name="itemsPerPage">Number of items for single page.</param>
        /// <param name="orderbyFunc">Selector for parameter to sort sequence.</param>
        /// <param name="selector">Function to test each element for condition.</param>
        /// <returns>List of filtered and paged items.</returns>
        IEnumerable<ItemType> GetPageOrderedByDescending<T>(Int32 pageNum,
                                                            Int32 itemsPerPage,
                                                            Func<ItemType, T> orderbyFunc,
                                                            Func<ItemType, Boolean> selector);

        /// <summary>
        /// Get list of filtered items with paging and ordered by ascending.
        /// </summary>
        /// <param name="pageNum">Number of current page.</param>
        /// <param name="itemsPerPage">Number of items for single page.</param>
        /// <param name="orderbyFunc">Selector for parameter to sort sequence.</param>
        /// <param name="selector">Function to test each element for condition.</param>
        /// <returns>List of filtered and paged items.</returns>
        IEnumerable<ItemType> GetPageOrderedByAscending<T>(Int32 pageNum,
                                                           Int32 itemsPerPage,
                                                           Func<ItemType, T> orderbyFunc,
                                                           Func<ItemType, Boolean> selector);

        /// <summary>
        /// Strict search of item. It means if search result equals to exception state, then ExceptionToThrow will be thrown.
        /// </summary>
        /// <typeparam name="ExceptionToThrow">The type of the exception to throw.</typeparam>
        /// <param name="id">The id.</param>
        /// <param name="exceptionState">State of the exception.</param>
        /// <returns></returns>
        /// <exception cref="ExceptionToThrow">Type of exception to be thrown.</exception>
        ItemType FindStrict<ExceptionToThrow>(Guid id, SearchState exceptionState)
            where ExceptionToThrow : Exception, new();
    }
}