﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;
using System.Web.Security;
using BusinessEntities;
using DAL_Interfaces;

namespace MembershipRoutines
{
    public sealed class WebSecurity
    {
        public static HttpContextBase Context
        {
            get { return new HttpContextWrapper(HttpContext.Current); }
        }

        public static HttpRequestBase Request
        {
            get { return Context.Request; }
        }

        public static HttpResponseBase Response
        {
            get { return Context.Response; }
        }

        public static System.Security.Principal.IPrincipal User
        {
            get { return Context.User; }
        }

        public static bool IsAuthenticated
        {
            get { return User.Identity.IsAuthenticated; }
        }

        public static MembershipCreateStatus Register(string Username, string Password,
                                                      string Email, bool IsApproved,
                                                      string FirstName, string LastName)
        {
            MembershipCreateStatus CreateStatus;
            Membership.CreateUser(Username, Password, Email, null, null,
                                  IsApproved, Guid.NewGuid(), out CreateStatus);

            if (CreateStatus == MembershipCreateStatus.Success)
            {
                IDatabase db = ((CodeFirstMembershipProvider) System.Web.Security.Membership.Provider).Database;
                User user = db.UserRepository.FindByLogin(Username);
                user.FirstName = FirstName;
                user.LastName = LastName;
                db.UserRepository.AddOrUpdate(user);
            }

            if (IsApproved)
            {
                FormsAuthentication.SetAuthCookie(Username, false);
            }
            return CreateStatus;
        }

        public static Boolean Login(string Username,
                                    string Password,
                                    bool persistCookie = false)
        {

            bool success = Membership.ValidateUser(Username, Password);
            if (success)
            {
                FormsAuthentication.SetAuthCookie(Username, persistCookie);
            }
            return success;

        }

        public static void Logout()
        {
            FormsAuthentication.SignOut();
        }

        public static MembershipUser GetUser(string Username)
        {
            return Membership.GetUser(Username);
        }

        public static bool ChangePassword(string userName, string currentPassword, string newPassword)
        {
            bool success = false;
            try
            {
                MembershipUser currentUser = Membership.GetUser(userName, true);
                success = currentUser.ChangePassword(currentPassword, newPassword);
            }
            catch (ArgumentException)
            {

            }
            return success;
        }

        public static bool DeleteUser(string Username)
        {
            return Membership.DeleteUser(Username);
        }

        public static int GetUserId(string userName)
        {
            MembershipUser user = Membership.GetUser(userName);
            return (int) user.ProviderUserKey;
        }

        public static string CreateAccount(string userName, string password, bool requireConfirmationToken = false)
        {
            CodeFirstMembershipProvider provider = (CodeFirstMembershipProvider) Membership.Provider;
            return provider.CreateAccount(userName, password, requireConfirmationToken);
        }

        public static string CreateUserAndAccount(string userName, string password,
                                                  object propertyValues = null,
                                                  bool requireConfirmationToken = false)
        {
            CodeFirstMembershipProvider provider = (CodeFirstMembershipProvider) Membership.Provider;
            IDictionary<string, object> values = null;
            if (propertyValues != null)
            {
                values = new RouteValueDictionary(propertyValues);
            }
            return provider.CreateUserAndAccount(userName, password, requireConfirmationToken, values);
        }

        public static List<MembershipUser> FindUsersByEmail(string Email, int PageIndex, int PageSize)
        {
            int totalRecords;
            return Membership
                .FindUsersByEmail(Email, PageIndex, PageSize, out totalRecords)
                .Cast<MembershipUser>().ToList();
        }

        public static List<MembershipUser> FindUsersByName(string Username, int PageIndex, int PageSize)
        {
            int totalRecords;
            return Membership
                .FindUsersByName(Username, PageIndex, PageSize, out totalRecords)
                .Cast<MembershipUser>().ToList();
        }

        public static List<MembershipUser> GetAllUsers(int PageIndex, int PageSize)
        {
            int totalRecords;
            return Membership
                .GetAllUsers(PageIndex, PageSize, out totalRecords)
                .Cast<MembershipUser>().ToList();
        }
    }
}