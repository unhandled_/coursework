﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Security;
using BusinessEntities;
using DAL_Interfaces;

namespace MembershipRoutines
{
    public class CodeFirstRoleProvider : RoleProvider
    {
        public IDatabase Database { get; set; }

        public override string ApplicationName
        {
            get { return "Test"; }
            set { }
        }

        public override bool RoleExists(string roleName)
        {
            if (string.IsNullOrEmpty(roleName))
            {
                return false;
            }

            Role Role = Database.UserRoleRepository.FindByName(roleName);
            return Role != null;
        }

        public override bool IsUserInRole(string username, string roleName)
        {
            if (string.IsNullOrEmpty(username))
            {
                return false;
            }
            if (string.IsNullOrEmpty(roleName))
            {
                return false;
            }
            User User = Database.UserRepository.FindByLogin(username);
            if (User == null)
            {
                return false;
            }
            Role Role = Database.UserRoleRepository.FindByName(roleName);
            if (Role == null)
            {
                return false;
            }
            return User.Roles.Contains(Role);
        }

        public override string[] GetAllRoles()
        {
            return Database.UserRoleRepository.Select(r => r.RoleName).ToArray();
        }

        public override string[] GetUsersInRole(string roleName)
        {
            if (string.IsNullOrEmpty(roleName))
            {
                return null;
            }

            Role Role = Database.UserRoleRepository.FindByName(roleName);
            return Role != null ? Role.Users.Select(Usr => Usr.Login).ToArray() : null;
        }

        public override string[] GetRolesForUser(string username)
        {
            if (string.IsNullOrEmpty(username))
            {
                return null;
            }
            if (Database == null)
            {
                return new string[0];
            }
            User User = Database.UserRepository.FindByLogin(username);
            return User != null ? User.Roles.Select(Rl => Rl.RoleName).ToArray() : null;
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            if (string.IsNullOrEmpty(roleName))
            {
                return null;
            }

            if (string.IsNullOrEmpty(usernameToMatch))
            {
                return null;
            }

            return (from Rl in Database.UserRoleRepository.All()
                    from Usr in Rl.Users
                    where Rl.RoleName == roleName && Usr.Login.Contains(usernameToMatch)
                    select Usr.Login).ToArray();
        }

        public override void CreateRole(string roleName)
        {
            if (!string.IsNullOrEmpty(roleName))
            {
                Role Role = Database.UserRoleRepository.FindByName(roleName);
                if (Role == null)
                {
                    Role NewRole = new Role
                        {
                            RoleId = Guid.NewGuid(),
                            RoleName = roleName
                        };
                    Database.UserRoleRepository.Add(NewRole);
                }
            }
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            if (string.IsNullOrEmpty(roleName))
            {
                return false;
            }
            Role Role = Database.UserRoleRepository.FindByName(roleName);
            if (Role == null)
            {
                return false;
            }
            if (throwOnPopulatedRole)
            {
                if (Role.Users.Any())
                {
                    return false;
                }
            }
            else
            {
                Role.Users.Clear();
            }
            Database.UserRoleRepository.Remove(Role);
            return true;
        }

        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            List<User> Users = Database.UserRepository.Where(Usr => usernames.Contains(Usr.Login)).ToList();
            List<Role> Roles = Database.UserRoleRepository.Where(Rl => roleNames.Contains(Rl.RoleName)).ToList();
            foreach (User user in Users)
            {
                foreach (Role role in Roles)
                {
                    if (!user.Roles.Contains(role))
                    {
                        user.Roles.Add(role);
                    }
                }
            }
            Database.SaveChanges();
        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            foreach (String username in usernames)
            {
                User user = Database.UserRepository.FindByLogin(username);
                if (user != null)
                {
                    foreach (String roleName in roleNames)
                    {
                        Role role = user.Roles.FirstOrDefault(R => R.RoleName == roleName);
                        if (role != null)
                        {
                            user.Roles.Remove(role);
                        }
                    }
                }
            }
            Database.SaveChanges();
        }
    }
}