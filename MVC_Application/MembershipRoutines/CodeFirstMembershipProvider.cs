﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Web;
using System.Web.Security;
using BL_Interfaces.Exceptions;
using BusinessEntities;
using DAL_Interfaces;

namespace MembershipRoutines
{
    public class CodeFirstMembershipProvider : MembershipProvider
    {
        public IDatabase Database { get; set; }

        #region Properties

        private const int TokenSizeInBytes = 16;

        public override string ApplicationName
        {
            get { return "Test"; }
            set { }
        }

        public override int MaxInvalidPasswordAttempts
        {
            get { return 5; }
        }

        public override int MinRequiredNonAlphanumericCharacters
        {
            get { return 0; }
        }

        public override int MinRequiredPasswordLength
        {
            get { return 6; }
        }

        public override int PasswordAttemptWindow
        {
            get { return 0; }
        }

        public override MembershipPasswordFormat PasswordFormat
        {
            get { return MembershipPasswordFormat.Hashed; }
        }

        public override string PasswordStrengthRegularExpression
        {
            get { return String.Empty; }
        }

        public override bool RequiresUniqueEmail
        {
            get { return true; }
        }

        #endregion

        #region Functions

        public override MembershipUser CreateUser(string username, string password, string email,
                                                  string passwordQuestion, string passwordAnswer, bool isApproved,
                                                  object providerUserKey, out MembershipCreateStatus status)
        {
            CheckDatabase();
            if (string.IsNullOrEmpty(username))
            {
                status = MembershipCreateStatus.InvalidUserName;
                return null;
            }
            if (string.IsNullOrEmpty(password))
            {
                status = MembershipCreateStatus.InvalidPassword;
                return null;
            }
            if (string.IsNullOrEmpty(email))
            {
                status = MembershipCreateStatus.InvalidEmail;
                return null;
            }
            string HashedPassword = Crypto.HashPassword(password);
            if (HashedPassword.Length > 128)
            {
                status = MembershipCreateStatus.InvalidPassword;
                return null;
            }
            if (Database.UserRepository.FindByLogin(username) != null)
            {
                status = MembershipCreateStatus.DuplicateUserName;
                return null;
            }
            if (Database.UserRepository.FindByEmail(email) != null)
            {
                status = MembershipCreateStatus.DuplicateEmail;
                return null;
            }
            User NewUser = new User
                {
                    UserId = Guid.NewGuid(),
                    Login = username,
                    Password = HashedPassword,
                    IsApproved = isApproved,
                    Email = email,
                    CreateDate = DateTime.UtcNow,
                    LastPasswordChangedDate = DateTime.UtcNow,
                    PasswordFailuresSinceLastSuccess = 0,
                    LastLoginDate = DateTime.UtcNow,
                    LastActivityDate = DateTime.UtcNow,
                    LastLockoutDate = DateTime.UtcNow,
                    IsLockedOut = false,
                    LastPasswordFailureDate = DateTime.UtcNow,
                    ConfirmationToken = GenerateToken(),
                };
            AddOrUpdateUser(NewUser);
            status = MembershipCreateStatus.Success;
            return new MembershipUser(Membership.Provider.Name, NewUser.Login, NewUser.UserId,
                                          NewUser.Email, null, null, NewUser.IsApproved,
                                          NewUser.IsLockedOut, NewUser.CreateDate ?? DateTime.Now,
                                          NewUser.LastLoginDate ?? DateTime.Now,
                                          NewUser.LastActivityDate ?? DateTime.Now,
                                          NewUser.LastPasswordChangedDate ?? DateTime.Now,
                                          NewUser.LastLockoutDate ?? DateTime.Now);
        }

        private void CheckDatabase()
        {
            if (Database == null)
            {
                throw new DatabaseFailedException(null);
            }
        }

        public string CreateUserAndAccount(string userName, string password, bool requireConfirmation,
                                           IDictionary<string, object> values)
        {
            CheckDatabase();
            return CreateAccount(userName, password, requireConfirmation);
        }

        public override bool ValidateUser(string username, string password)
        {
            CheckDatabase();
            if (string.IsNullOrEmpty(username))
            {
                return false;
            }
            if (string.IsNullOrEmpty(password))
            {
                return false;
            }
            User User = Database.UserRepository.FindByLogin(username);
            if (User == null)
            {
                return false;
            }
            if (User.IsLockedOut)
            {
                return false;
            }
            String HashedPassword = User.Password;
            Boolean VerificationSucceeded = (HashedPassword != null
                                             && Crypto.VerifyHashedPassword(HashedPassword, password));
            if (VerificationSucceeded)
            {
                User.PasswordFailuresSinceLastSuccess = 0;
                User.LastLoginDate = DateTime.UtcNow;
                User.LastActivityDate = DateTime.UtcNow;
            }
            else
            {
                int Failures = User.PasswordFailuresSinceLastSuccess;
                if (Failures < MaxInvalidPasswordAttempts)
                {
                    User.PasswordFailuresSinceLastSuccess += 1;
                    User.LastPasswordFailureDate = DateTime.UtcNow;
                }
                else if (Failures >= MaxInvalidPasswordAttempts)
                {
                    User.LastPasswordFailureDate = DateTime.UtcNow;
                    User.LastLockoutDate = DateTime.UtcNow;
                    User.IsLockedOut = true;
                }
            }
            AddOrUpdateUser(User);
            return VerificationSucceeded;
        }

        public override MembershipUser GetUser(string username, bool userIsOnline)
        {
            CheckDatabase();
            if (string.IsNullOrEmpty(username))
            {
                return null;
            }
            User User = Database.UserRepository.FindByLogin(username);
            if (User != null)
            {
                if (userIsOnline)
                {
                    User.LastActivityDate = DateTime.UtcNow;
                    AddOrUpdateUser(User);
                }
                return new MembershipUser(Membership.Provider.Name, User.Login, User.UserId,
                                          User.Email, null, null, User.IsApproved, 
                                          User.IsLockedOut, User.CreateDate ?? DateTime.Now,
                                          User.LastLoginDate ?? DateTime.Now,
                                          User.LastActivityDate ?? DateTime.Now,
                                          User.LastPasswordChangedDate ?? DateTime.Now,
                                          User.LastLockoutDate ?? DateTime.Now);
            }
            return null;
        }

        public override MembershipUser GetUser(object providerUserKey, bool userIsOnline)
        {
            CheckDatabase();
            if (!(providerUserKey is Guid))
            {
                return null;
            }
            User User = Database.UserRepository.Find((Guid)providerUserKey);
            if (User != null)
            {
                if (userIsOnline)
                {
                    User.LastActivityDate = DateTime.UtcNow;
                    AddOrUpdateUser(User);
                }
                return new MembershipUser(Membership.Provider.Name, User.Login, User.UserId,
                                          User.Email, null, null, User.IsApproved,
                                          User.IsLockedOut, User.CreateDate ?? DateTime.Now,
                                          User.LastLoginDate ?? DateTime.Now, 
                                          User.LastActivityDate ?? DateTime.Now,
                                          User.LastPasswordChangedDate ?? DateTime.Now,
                                          User.LastLockoutDate ?? DateTime.Now);
            }
            return null;
        }

        public override bool ChangePassword(string username, string oldPassword, string newPassword)
        {
            CheckDatabase();
            if (string.IsNullOrEmpty(username))
            {
                return false;
            }
            if (string.IsNullOrEmpty(oldPassword))
            {
                return false;
            }
            if (string.IsNullOrEmpty(newPassword))
            {
                return false;
            }
            User User = Database.UserRepository.FindByLogin(username);
            if (User == null)
            {
                return false;
            }
            String HashedPassword = User.Password;
            Boolean VerificationSucceeded = (HashedPassword != null
                                             && Crypto.VerifyHashedPassword(HashedPassword, oldPassword));
            if (VerificationSucceeded)
            {
                User.PasswordFailuresSinceLastSuccess = 0;
            }
            else
            {
                int Failures = User.PasswordFailuresSinceLastSuccess;
                if (Failures < MaxInvalidPasswordAttempts)
                {
                    User.PasswordFailuresSinceLastSuccess += 1;
                    User.LastPasswordFailureDate = DateTime.UtcNow;
                }
                else if (Failures >= MaxInvalidPasswordAttempts)
                {
                    User.LastPasswordFailureDate = DateTime.UtcNow;
                    User.LastLockoutDate = DateTime.UtcNow;
                    User.IsLockedOut = true;
                }
                AddOrUpdateUser(User);
                return false;
            }
            String NewHashedPassword = Crypto.HashPassword(newPassword);
            if (NewHashedPassword.Length > 128)
            {
                return false;
            }
            User.Password = NewHashedPassword;
            User.LastPasswordChangedDate = DateTime.UtcNow;
            AddOrUpdateUser(User);
            return true;
        }

        public override bool UnlockUser(string userName)
        {
            CheckDatabase();
            User User = Database.UserRepository.FindByLogin(userName);
            if (User != null)
            {
                User.IsLockedOut = false;
                User.PasswordFailuresSinceLastSuccess = 0;
                AddOrUpdateUser(User);
                return true;
            }
            return false;
        }

        private void AddOrUpdateUser(User User)
        {
            try
            {
                Database.UserRepository.AddOrUpdate(User);
            }
            catch (Exception e)
            {
                throw new DatabaseFailedException(e);
            }
        }

        public override int GetNumberOfUsersOnline()
        {
            CheckDatabase();
            DateTime DateActive =
                DateTime.UtcNow.Subtract(
                    TimeSpan.FromMinutes(Convert.ToDouble(Membership.UserIsOnlineTimeWindow)));
            return Database.UserRepository.Count(usr => usr.LastActivityDate > DateActive);
        }

        public override bool DeleteUser(string username, bool deleteAllRelatedData)
        {
            CheckDatabase();
            if (string.IsNullOrEmpty(username))
            {
                return false;
            }
            User User = Database.UserRepository.FindByLogin(username);
            if (User != null)
            {
                try
                {
                    Database.UserRepository.Remove(User);
                }
                catch
                {
                    return false;
                }
                return true;
            }
            return false;
        }

        public override string GetUserNameByEmail(string email)
        {
            CheckDatabase();
            var user = Database.UserRepository.FindByEmail(email);
            return user == null ? String.Empty : user.Login;
        }

        public override MembershipUserCollection FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize,
                                                                  out int totalRecords)
        {
            CheckDatabase();
            MembershipUserCollection MembershipUsers = new MembershipUserCollection();
            var user = Database.UserRepository.FindByEmail(emailToMatch);

            if (user != null)
            {
                MembershipUsers.Add(new MembershipUser(Membership.Provider.Name, user.Login,
                                                       user.UserId, user.Email, null, 
                                                       null, user.IsApproved, user.IsLockedOut, 
                                                       user.CreateDate ?? DateTime.Now,
                                                       user.LastLoginDate ?? DateTime.Now,
                                                       user.LastActivityDate ?? DateTime.Now,
                                                       user.LastPasswordChangedDate ?? DateTime.Now,
                                                       user.LastLockoutDate ?? DateTime.Now));
            }
            totalRecords = user == null ? 0 : 1;
            return MembershipUsers;
        }

        public override MembershipUserCollection FindUsersByName(string usernameToMatch, int pageIndex, int pageSize,
                                                                 out int totalRecords)
        {
            CheckDatabase();
            MembershipUserCollection MembershipUsers = new MembershipUserCollection();
            var user = Database.UserRepository.FindByEmail(usernameToMatch);

            if (user != null)
            {
                MembershipUsers.Add(new MembershipUser(Membership.Provider.Name, user.Login,
                                                       user.UserId, user.Email, null, null, 
                                                       user.IsApproved, user.IsLockedOut, 
                                                       user.CreateDate ?? DateTime.Now,
                                                       user.LastLoginDate ?? DateTime.Now, 
                                                       user.LastActivityDate ?? DateTime.Now,
                                                       user.LastPasswordChangedDate ?? DateTime.Now,
                                                       user.LastLockoutDate ?? DateTime.Now));
            }
            totalRecords = user == null ? 0 : 1;
            return MembershipUsers;
        }

        public override MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords)
        {
            CheckDatabase();
            MembershipUserCollection MembershipUsers = new MembershipUserCollection();
            totalRecords = Database.UserRepository.Count();
            IEnumerable<User> Users =
                Database.UserRepository.All()
                    .OrderBy(Usrn => Usrn.Login)
                    .Skip(pageIndex * pageSize)
                    .Take(pageSize);
            foreach (User user in Users)
            {
                MembershipUsers.Add(new MembershipUser(Membership.Provider.Name,
                                                       user.Login, user.UserId, user.Email, 
                                                       null, null, user.IsApproved, user.IsLockedOut,
                                                       user.CreateDate ?? DateTime.Now, 
                                                       user.LastLoginDate ?? DateTime.Now,
                                                       user.LastActivityDate ?? DateTime.Now,
                                                       user.LastPasswordChangedDate ?? DateTime.Now,
                                                       user.LastLockoutDate ?? DateTime.Now));
            }
            return MembershipUsers;
        }

        public string CreateAccount(string userName, string password, bool requireConfirmationToken)
        {
            CheckDatabase();
            if (string.IsNullOrEmpty(userName))
            {
                throw new MembershipCreateUserException(MembershipCreateStatus.InvalidUserName);
            }
            if (string.IsNullOrEmpty(password))
            {
                throw new MembershipCreateUserException(MembershipCreateStatus.InvalidPassword);
            }
            string hashedPassword = Crypto.HashPassword(password);
            if (hashedPassword.Length > 128)
            {
                throw new MembershipCreateUserException(MembershipCreateStatus.InvalidPassword);
            }
            if (Database.UserRepository.FindByLogin(userName) != null)
            {
                throw new MembershipCreateUserException(MembershipCreateStatus.DuplicateUserName);
            }
            string token = string.Empty;
            if (requireConfirmationToken)
            {
                token = GenerateToken();
            }
            User NewUser = new User
                {
                    UserId = Guid.NewGuid(),
                    Login = userName,
                    Password = hashedPassword,
                    IsApproved = !requireConfirmationToken,
                    Email = string.Empty,
                    CreateDate = DateTime.UtcNow,
                    LastPasswordChangedDate = DateTime.UtcNow,
                    PasswordFailuresSinceLastSuccess = 0,
                    LastLoginDate = DateTime.UtcNow,
                    LastActivityDate = DateTime.UtcNow,
                    LastLockoutDate = DateTime.UtcNow,
                    IsLockedOut = false,
                    LastPasswordFailureDate = DateTime.UtcNow,
                    ConfirmationToken = token
                };
            AddOrUpdateUser(NewUser);
            return token;
        }

        private static string GenerateToken()
        {
            using (var prng = new RNGCryptoServiceProvider())
            {
                return GenerateToken(prng);
            }
        }

        internal static string GenerateToken(RandomNumberGenerator generator)
        {
            byte[] tokenBytes = new byte[TokenSizeInBytes];
            generator.GetBytes(tokenBytes);
            return HttpServerUtility.UrlTokenEncode(tokenBytes);
        }

        #endregion

        #region Not Supported

        //CodeFirstMembershipProvider does not support password retrieval scenarios.
        public override bool EnablePasswordRetrieval
        {
            get { return false; }
        }

        public override string GetPassword(string username, string answer)
        {
            throw new NotSupportedException("Consider using methods from WebSecurity module.");
        }

        //CodeFirstMembershipProvider does not support password reset scenarios.
        public override bool EnablePasswordReset
        {
            get { return false; }
        }

        public override string ResetPassword(string username, string answer)
        {
            throw new NotSupportedException("Consider using methods from WebSecurity module.");
        }

        //CodeFirstMembershipProvider does not support question and answer scenarios.
        public override bool RequiresQuestionAndAnswer
        {
            get { return false; }
        }

        public override bool ChangePasswordQuestionAndAnswer(string username, string password,
                                                             string newPasswordQuestion, string newPasswordAnswer)
        {
            throw new NotSupportedException("Consider using methods from WebSecurity module.");
        }

        //CodeFirstMembershipProvider does not support UpdateUser because this method is useless.
        public override void UpdateUser(MembershipUser user)
        {
            throw new NotSupportedException();
        }

        #endregion
    }
}