﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using BL_Interfaces;

namespace MVC_UI
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        protected void Application_AcquireRequestState(object sender, EventArgs e)
        {
            CultureInfo currentCulture = null;
            if (Request.Cookies["language"] != null)
            {
                currentCulture = new CultureInfo(Request.Cookies["language"].Value);
            }
            if (currentCulture == null)
            {
                string langName = "en";
                if (HttpContext.Current.Request.UserLanguages != null && HttpContext.Current.Request.UserLanguages.Length != 0)
                {
                    langName = HttpContext.Current.Request.UserLanguages[0].Substring(0, 2);
                }
                currentCulture = new CultureInfo(langName);
                Response.AppendCookie(new HttpCookie("language", langName));
            }
            Thread.CurrentThread.CurrentUICulture = currentCulture;
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(currentCulture.Name);
        }
    }
}