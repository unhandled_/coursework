using System;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Security;
using BL_Interfaces;
using DAL_Interfaces;
using MembershipRoutines;
using Ninject;
using Ninject.Modules;

namespace MVC_UI
{
    public class LogicsFactory
    {
        private StandardKernel kernel;

        private IKernel Kernel
        {
            get
            {
                if (kernel == null)
                {
                    Type databaseType = GetCustomType("DataAccessLayerLibraryPath", typeof (IDatabase));
                    Type logicsType = GetCustomType("LogicsLibraryPath", typeof (ILogics));
                    kernel = new StandardKernel(new ConfigModule(databaseType, logicsType));
                }
                return kernel;
            }
        }

        public ILogics GetLogics()
        {
            ILogics logics = HttpContext.Current.Items["Logics"] as ILogics;

            if (logics == null)
            {
                logics = Kernel.Get<ILogics>();
                HttpContext.Current.Items["Logics"] = logics;
            }
            return logics;
        }

        public void InitializeLogics()
        {
            GetLogics();
        }

        private static Type GetCustomType(String connectionStringName, Type assignableType)
        {
            String assemblyPath = ConfigurationManager.ConnectionStrings[connectionStringName].ToString();
            Assembly assembly = Assembly.LoadFrom(assemblyPath);
            Type[] types = assembly.GetTypes();
            return types.First(assignableType.IsAssignableFrom);
        }

        private class ConfigModule : NinjectModule
        {
            private readonly Type databaseType;
            private readonly Type logicsType;

            public ConfigModule(Type databaseType, Type logicsType)
            {
                this.databaseType = databaseType;
                this.logicsType = logicsType;
            }

            public override void Load()
            {
                Bind<IDatabase>().To(databaseType);
                Bind<ILogics>().To(logicsType);
            }
        }
    }
}