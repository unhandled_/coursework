﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace MVC_UI
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Main",
                url: "",
                defaults: new
                {controller = "Shared", action = "Index"}
                );
            
            routes.MapRoute(
                name: "User",
                url: "User/{action}/{name}",
                defaults: new {controller = "User", action = "Index", name = UrlParameter.Optional}
                );

            routes.MapRoute(
                name: "AdminUsers",
                url: "Admin/UsersList/{page}",
                defaults: new {controller = "Admin", action = "UsersList"});

            routes.MapRoute(
                name: "Admin",
                url: "Admin/{action}/{name}",
                defaults: new {controller = "Admin", action = "Index", name = UrlParameter.Optional});

            routes.MapRoute(
                name: "Shared",
                url: "{action}/{name}",
                defaults: new {controller = "Shared", name = UrlParameter.Optional});
        }
    }
}