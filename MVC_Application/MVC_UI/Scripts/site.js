﻿/** Theme
 */

$(function () {
    $("#black").click(makeblack);
    $("#white").click(makewhite);
    if ($.cookie("theme") == "black")
        makeblack();
    else
        makewhite();

    function makeblack() {
        $.cookie("theme", "black", { path: "/"});
        $(".navbar").addClass("navbar-inverse");
        $("body").removeClass("body-white");
        $("body").addClass("body-black");
    }
    function makewhite() {
        $.cookie("theme", "white", { path: "/"});
        $(".navbar").removeClass("navbar-inverse");
        $("body").removeClass("body-black");
        $("body").addClass("body-white");
        
    }
})

/** Function for like-button
 */
$(function() {
    $(document).on("click", ".like", function(e) {
        e.preventDefault();
        var thislike = $(this);
        $.ajax({
            url: $(this).attr("data-url"),
            success: function(data) {
                thislike.find("b").text(data);
            }
        });
        return false;
    });
});
/*

$(function () {
    $(".like").click(function () {
        var thislike = $(this);
        $.ajax({
            url: $(this).attr("data-url"),
            success: function (data) {
                thislike.find("b").text(data);
            }
        })
    })
})
*/

/** Autosubmit
 */

$(function () {
    var interval = setInterval(function () {
        $("#save").click();
    }, 300000)
})

//$(function () {
//    $(document).on("click", ".edit", function (e) {
//        e.preventDefault();
//        $('#editUserBody').load($(this).attr("data-url"), {}, function () {
//            $('form').removeData('validator');
//            $('form').removeData('unobtrusiveValidation');
//            $.validator.unobtrusive.parse('form');
//        });
//    });
//});
