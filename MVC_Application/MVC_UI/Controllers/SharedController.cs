﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BusinessEntities;
using MVC_UI.Models;
using System.Globalization;
using System.Web.Security;

namespace MVC_UI.Controllers
{
    public class SharedController : BasicController
    {
        //
        // GET: /Shared/
        public ActionResult Index()
        {
            MainModel model = new MainModel();
            model.RecentTasks = Logics.TaskAC.GetPageOrderedByDescending(0, 10, t => t.PostDate, t => true).ToList();

            return View("Main", model);
        }

        public ActionResult Popular()
        {
            List<Task> popularTasks = new List<Task>();
            return View("Tasks", popularTasks);
        }

        public ActionResult Users(string name)
        {
            return View("User", Logics.UserAC.Where(u => u.Login == name).SingleOrDefault());
        }


        public ActionResult Task(string name)
        {
            Task task = Logics.TaskAC.Where(t => t.NameForLink == name).SingleOrDefault();
            return View("Task", new TaskWithAnswerModel(task));
        }

        public PartialViewResult ToAnswer(TaskWithAnswerModel task)
        {
            try
            {
                //if (Logics.CurrentUser.Tasks.Where(t => t.Id == task.Id).Count() != 0)
                //{
                //    ModelState.AddModelError("", ControllersResources.ValidationStrings.YoursTask);
                //}
                if (Logics.TaskAC.TrySolve(task.Id, task.MyAnswer))
                {
                    ViewData["goodans"] = ControllersResources.ValidationStrings.RightAns;
                }
                else
                {
                    ModelState.AddModelError("", ControllersResources.ValidationStrings.WrongAns);
                }
                return PartialView("Answer", task);
            }
            catch(Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return PartialView("Answer", task);
            }
        }

        public ActionResult Tag(string name)
        {
            return View("Tasks", Logics.TaskAC.Where(t => t.Tags
                .FirstOrDefault(tag => tag.Text.Contains(name)) != null).ToList());
                //.Tags.Select(g => g.Text).Contains(name)));
        }

        public ActionResult Technology(string name)
        {
            return View("Tasks", Logics.TaskAC.Where(t => t.Technology.Name == name).ToList());
        }

        public ContentResult Like(Guid id)
        {
            return Content(Logics.TaskAC.Like(id).ToString());
        }

        public ActionResult Search(String searchText)
        {
            return View("Tasks", Logics.TaskAC.Find(searchText));
        }

        public ActionResult Language(string returnUrl, string language)
        {
            Response.AppendCookie( new HttpCookie("language", language));
            return Redirect(returnUrl);
        }

        #region LOGIN

        public ActionResult Login()
        {
            if (Logics.CurrentUser == null)
                return View("Login", new LoginModel());
            else
                return RedirectToAction("UserTasks", "User");
        }
        
        [HttpPost]
        public ActionResult SignIn(LoginModel model)
        {
            try
            {
                if (Logics.UserAC.FindByLogin(model.Login, true).IsRemoved)
                {
                    return RedirectToAction("RemovedUser");
                }
                if (!Logics.UserAC.FindByLogin(model.Login, true).IsApproved)
                {
                    return RedirectToAction("Authentication");
                }
                Logics.UserAC.LogIn(model.Login, model.Password);
                FormsAuthentication.SetAuthCookie(model.Login, model.RememberMe);
                return RedirectToAction("UserTasks", "User");
            }
            catch(Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View("Login", model);
            }
        }

        public ActionResult RemovedUser()
        {
            return View("RemovedUser", new RemovedUserModel());
        }

        [HttpPost]
        public ActionResult Restore(RemovedUserModel model)
        {
            try
            {
                Logics.UserAC.RecoverByUser(model.Login, model.Password);
                FormsAuthentication.SetAuthCookie(model.Login, false);
                return RedirectToAction("UserTasks", "User");
            }
            catch(Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View("RemovedUser", model);
            }
        }

        public ActionResult Registration()
        {
            return View("Register", new RegisterModel());
        }

        [HttpPost]
        public ActionResult Register(RegisterModel model)
        {
            try
            {
                Logics.UserAC.Register(model.Login, model.Password, model.Email, false);
                return View("Authentication", new AuthenticationModel());
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View("Register", model);
            }
        }

        public ActionResult Authenticate(AuthenticationModel model)
        {
            try
            {
                if (Logics.UserAC.Approve(model.Login, model.Key))
                {
                    FormsAuthentication.SetAuthCookie(model.Login, false);
                    return RedirectToAction("UserTasks", "User");
                }
                else
                {
                    throw new Exception(ControllersResources.ValidationStrings.WrongKey);
                }
            }
            catch(Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View("Authentication", model);
            }
        }

        public ActionResult Authentication()
        {
            return View();
        }

        public JsonResult ValidateLogin(String login)
        {
            if (Logics.UserAC.FindByLogin(login, false) != null)
                return Json(ControllersResources.ValidationStrings.LoginFound, JsonRequestBehavior.AllowGet);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ValidateEmail(String email)
        {
            if (Logics.UserAC.FindByMail(email, false) != null)
                return Json(ControllersResources.ValidationStrings.EmailFound, JsonRequestBehavior.AllowGet);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        #endregion

    }
}
