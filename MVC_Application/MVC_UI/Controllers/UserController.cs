﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BL_Interfaces.Exceptions;
using BusinessEntities;
using MVC_UI.Models;
using MarkdownSharp;
using System.Web.Security;
using System.Globalization;

namespace MVC_UI.Controllers
{
    [Authorize]
    public class UserController : BasicController
    {
        #region EDIT_USER

        public ActionResult Settings()
        {
            return View("Settings", new UserModel(Logics.CurrentUser));
        }

        [HttpPost]
        public ActionResult SaveUser(UserModel model, String oldpassword, String newpassword)
        {
            ModelState["Password"].Errors.Clear();

             try
             {
               Logics.UserAC.Edit(Logics.CurrentUser.UserId, oldpassword, newpassword, model.Email);
               ViewData["success"] = ControllersResources.ValidationStrings.Saved;                
               if (Membership.ValidateUser(Logics.CurrentUser.Login, oldpassword))
               {
                   Logics.UserAC.Edit(Logics.CurrentUser.UserId, model.Login, newpassword, model.Email);
                   Logics.UserAC.SetLanguage(model.Login, model.LanguageLocale);
                   ViewData["success"] = ControllersResources.ValidationStrings.Saved;
               }
               else
               {
                    throw new AuthorizationFailedExpcetion();
               }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
            }
            return View("Settings", model);
        }

        public ActionResult DeleteAccount()
        {
            Logics.UserAC.RemoveByUser((Logics.CurrentUser.Login));
            FormsAuthentication.SignOut();
            return RedirectToAction("RemovedUser", "Shared");
        }

        #endregion

        #region EDIT_DRAFT


        [AcceptVerbs(HttpVerbs.Post)]
        [ValidateInput(false)]
        public ActionResult Preview(String name)
        {
            Markdown md = new Markdown();
            name = md.Transform(name);
            return Content(name);
        }

        public ActionResult CreateTask()
        {
            Draft currentDraft = new Draft();
            return View("EditDraft", new DraftModel(currentDraft, null, null));
        }

        public ActionResult EditDraft(string name)
        {
            try
            {               
                Draft currentDraft = Logics.DraftAC.Where(t => t.NameForLink == name).SingleOrDefault();
                Task currentTask = Logics.TaskAC.Where(t => t.NameForLink == name).SingleOrDefault();
                if (currentDraft == null)
                {
                    currentDraft = Logics.DraftAC.Create(currentTask != null ? currentTask.Id : Guid.Empty);
                }
                if (ModelState.Keys.Contains("name"))
                    ModelState.Clear();
                return View("EditDraft", new DraftModel(currentDraft, null, null));
            }
            catch(Exception exception)
            {
                throw new HttpException(502, exception.Message);
            }
        }

        [HttpPost]
        public ActionResult SaveDraft(DraftModel draft)
        {
            Draft currentDraft = Logics.DraftAC.Find(draft.Id);
            if (currentDraft != null)
            {
                draft.AnswerList = currentDraft.Answers.Select(a => a.Text).ToList();
                draft.TagList = currentDraft.Tags.Select(a => a.Text).ToList();
            }
            if (draft.AnswerName != null)
            {
                draft.AnswerList.Add(draft.AnswerName);
            }
            if (draft.TagName != null)
            {
                draft.TagList.Add(draft.TagName);
            }
            try
            {
                if (currentDraft != null)
                {
                    Logics.DraftAC.Edit(draft.Id, draft.Description, draft.Text, draft.Name, draft.TechnologyName,
                        draft.LocaleLanguage, draft.TagList, draft.AnswerList);
                }
                else
                {
                    draft.Id = Logics.DraftAC.Create(draft.Description, draft.Text, draft.Name, draft.TechnologyName,
                                                     draft.LocaleLanguage, draft.TagList, draft.AnswerList).Id;
                    ModelState.Clear();

                }
                ViewData["success"] = ControllersResources.ValidationStrings.Saved;
                draft.AnswerName = null;
                draft.TagName = null;
                return View("EditDraft", draft);
            }
            catch(Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View("EditDraft", draft);
            }
        }

        public ActionResult PublishTask(Guid id)
        {
            ModelState.Clear();
            try
            {
                Logics.DraftAC.SaveChanges(id);
                ModelState.Clear();
                return RedirectToAction("UserTasks");
            }
            catch (Exception ex)
            {
                throw new HttpException(502, ex.Message);
            }
        }

        public ActionResult DeleteTag(String tag, Guid draftId)
        {
            Logics.DraftAC.RemoveTag(draftId, tag);
            return View("EditDraft", new DraftModel(Logics.DraftAC.Find(draftId), null, null));
        }

        public ActionResult DeleteAnswer(String answer, Guid draftId)
        {
            Logics.DraftAC.RemoveAnswer(draftId, answer);
            return View("EditDraft", new DraftModel(Logics.DraftAC.Find(draftId), null, null));
        }

        public JsonResult ValidateTaskName(String Name, Guid OriginalId, Guid Id)
        {
            Draft validateDraft = Logics.DraftAC.Where(t => t.Id == Id).SingleOrDefault();
            if (validateDraft == null)
            {
                if (Logics.TaskAC.Where(t => t.Name == Name).Count() == 0 && Logics.DraftAC.Where(t => t.Name == Name).Count() == 0)
                    return Json(true, JsonRequestBehavior.AllowGet);
                else
                    return Json(ControllersResources.ValidationStrings.ExistName, JsonRequestBehavior.AllowGet);
            }
            else if (OriginalId == Guid.Empty)
            {
                if (Logics.TaskAC.Where(t => t.Name == Name).Count() == 0 && Logics.DraftAC.Where(t => t.Name == Name && t.Id != Id).Count() == 0)
                    return Json(true, JsonRequestBehavior.AllowGet);
                else
                    return Json(ControllersResources.ValidationStrings.ExistName, JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (Logics.TaskAC.Where(t => t.Name == Name && t.Id != OriginalId).Count() == 0 && Logics.DraftAC.Where(t => t.Name == Name && t.Id != Id).Count() == 0)
                    return Json(true, JsonRequestBehavior.AllowGet);
                else
                    return Json(ControllersResources.ValidationStrings.ExistName, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult ValidateAnswer(String AnswerName, Guid Id)
        {
            Draft validateDraft = Logics.DraftAC.Where(t => t.Id == Id).SingleOrDefault();
            if (validateDraft != null && validateDraft.Answers.Select(a => a.Text).Contains(AnswerName))
                return Json(ControllersResources.ValidationStrings.ExistAnswer, JsonRequestBehavior.AllowGet);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ValidateTag(String TagName, Guid Id)
        {
            Draft validateDraft = Logics.DraftAC.Where(t => t.Id == Id).SingleOrDefault();
            if (validateDraft != null && validateDraft.Tags.Select(a => a.Text).Contains(TagName))
                return Json(ControllersResources.ValidationStrings.ExistTag, JsonRequestBehavior.AllowGet);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeleteTask(String name)
        {
            try
            {
                Task deletingTask = Logics.TaskAC.Where(t => t.NameForLink == name).SingleOrDefault();
                if (deletingTask == null)
                {
                    throw new TaskNotFoundException();
                }
                Logics.TaskAC.Remove(deletingTask.Id);
                return RedirectToAction("UserTasks");
            }
            catch(Exception ex)
            {
                throw new HttpException(502, ex.Message); 
            }
        }

        public ActionResult DeleteDraft(String name)
        {
            try
            {
                Draft deletingDraft = Logics.DraftAC.Where(t => t.NameForLink == name).SingleOrDefault();
                if (deletingDraft == null)
                {
                    throw new DraftNotFoundException();
                }
                Logics.DraftAC.Remove(deletingDraft.Id);
                return RedirectToAction("UserDrafts");
            }
            catch (Exception ex)
            {
                throw new HttpException(502, ex.Message);
            }
        }

        #endregion

        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Login", "Shared");
        }

        public ActionResult UserTasks()
        {
            return View("UserTasks", Logics.CurrentUser);
        }

        public ActionResult UserDrafts()
        {
            return View("UserDrafts", Logics.CurrentUser);
        }

        public ActionResult SolvedTasks()
        {
            return View("SolvedTasks", Logics.CurrentUser.SolvedTasks);
        }
    }
}
