﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using BL_Interfaces.Exceptions;
using BusinessEntities;
using BusinessLogics.Properties;
using ControllersResources;
using MVC_UI.Models;
using MVC_UI.Resources.Controllers;
using MVC_UI.Resources.Views;

namespace MVC_UI.Controllers
{
    public class AdminController : BasicController
    {
        [Authorize(Roles = "Administrator")]
        public ActionResult Index()
        {
            return View(Logics.UserAC.All());
        }

        public JsonResult Remove(String name)
        {
            var user = Logics.UserAC.FindByLogin(name, false);
            if(user == null)
            {
                return Json(new {status = "Error", value = new LoginNotFoundException().Message},
                            JsonRequestBehavior.AllowGet);
            }
            var message = user.IsRemoved ? AdminValidation.UserRecoverSuccess : AdminValidation.UserRemoveSuccess; 
            try
            {
                Logics.UserAC.RemoveOrRecoverByAdmin(name);
            }
            catch (Exception e)
            {
                return Json(new {status = "Error", value = e.Message}, JsonRequestBehavior.AllowGet);
            }
            return Json(new {status = "OK", value = message}, JsonRequestBehavior.AllowGet);
        }

        public JsonResult TechTypeahead(String name)
        {
            return Json(new {options = Logics.TechnologyAC.FindTechnologiesContainingText(name).Select(t => t.Name)},
                        JsonRequestBehavior.AllowGet);
        }

        public JsonResult TagTypeahead(String name)
        {
            return Json(new {options = Logics.TagAC.FindTagsContainingText(name).Select(t => t.Text)},
                        JsonRequestBehavior.AllowGet);
        }

        public JsonResult Approve(String name)
        {
            var user = Logics.UserAC.FindByLogin(name, false);
            if (user == null)
            {
                return Json(new { status = "Error", value = new LoginNotFoundException().Message },
                            JsonRequestBehavior.AllowGet);
            }
            var message = user.IsApproved ? AdminPageStrings.UserAlreadyApproved : AdminPageStrings.UserApproveSuccess;
            try
            {
                user.IsApproved = true;
                Logics.Database.UserRepository.AddOrUpdate(user);
            }
            catch (Exception e)
            {
                return Json(new { status = "Error", value = e.Message }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { status = "OK", value = message }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Edit(string name)
        {
            if (!Roles.IsUserInRole("Administrator"))
            {
                return Content(new NoRightsException().Message);
            }
            var user = Logics.UserAC.FindByLogin(name, false);
            var model = new UserModel(user)
                {
                    Languages = Logics.LanguageAC.All().Select(lang => new SelectListItem
                        {
                            Selected = user.Language != null && user.Language.Locale == lang.Locale,
                            Text = lang.Name,
                            Value = lang.Locale,
                        }).ToList(),
                        Password = user.Password.Substring(0, 10),
                        UserId = user.UserId,
                };
            return PartialView(model);
        }

        [HttpPost]
        public ActionResult Save(UserModel user)
        {
            var original = Logics.UserAC.Find(user.UserId);
            var substring = original.Password.Substring(0, 10);
            bool passwordChanged = user.Password != substring;
            try
            {
                Logics.UserAC.Edit(user.UserId, user.Login, passwordChanged ? user.Password : null, user.Email);
            }
            catch(Exception e)
            {
                throw new HttpException(502, e.Message);
            }
            return Content(AdminPageStrings.UserEditSuccess);
        }

        public JsonResult ValidateLogin(String login, Guid userId)
        {
            var user = Logics.UserAC.Find(userId);
            if (Logics.UserAC.FindByLogin(login, false) != null && user.Login != login)
            {
                return Json(ValidationStrings.LoginFound, JsonRequestBehavior.AllowGet);
            }
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ValidateEmail(String email, Guid userId)
        {
            var user = Logics.UserAC.Find(userId);
            if (Logics.UserAC.FindByMail(email, false) != null && user.Email != email)
            {
                return Json(ValidationStrings.EmailFound, JsonRequestBehavior.AllowGet);
            }
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ValidateLanguage(String languageLocale)
        {
            var language = Logics.LanguageAC.Find(languageLocale, false) ??
                           Logics.LanguageAC.FirstOrDefault(l => l.Name == languageLocale);
            if (language == null)
            {
                return Json(AdminValidation.LanguageNotFound, JsonRequestBehavior.AllowGet);
            }
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Resend(string name)
        {
            var user = Logics.UserAC.FindByLogin(name, false);
            if (user == null)
            {
                return Json(new {status = "Error", value = new LoginNotFoundException().Message},
                            JsonRequestBehavior.AllowGet);
            }
            var message = user.IsApproved ? AdminPageStrings.MailSendFailed : AdminPageStrings.MailSendSuccess;
            try
            {
                Logics.UserAC.SendVerification(name);
            }
            catch (Exception e)
            {
                return Json(new { status = "Error", value = e.Message }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { status = "OK", value = message }, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult UsersList(int page = 0)
        {
            const int items = 2;
            ViewData["Pager"] = GetPager(items, Logics.UserAC.Count(), page);
            ViewData["Page"] = page;
            var users = Logics.UserAC.GetPageOrderedByAscending(page, items, u => u.Login, u => true);
            return PartialView("List", users);
        }

        public ActionResult TagsCloud()
        {
            var httpCookie = Request.Cookies["language"];
            return PartialView(Logics.TagAC.GetMostPopular(20, httpCookie == null ? "ru" : httpCookie.Value));
        }

        public ActionResult Popular()
        {
            var tasks = Logics.TaskAC.GetPageOrderedByDescending(0, 8, t => t.Likes.Count, t => true);
            return PartialView("Popular", tasks);
        }

        public IEnumerable<Tuple<String, String>> GetPager(int itemsPerPage, int count, int page)
        {
            int pagesCount = (int)Math.Ceiling(1.0f * count / itemsPerPage);
            var res = new List<Tuple<string, string>>();
            if (count != 0)
            {
                if (page > 0 && page < pagesCount)
                {
                    res.Add(new Tuple<string, string>(AdminPageStrings.PrevPage, (page - 1).ToString()));
                }
                for (int i = 0; i < pagesCount; i++)
                {
                    res.Add(new Tuple<string, string>(i.ToString(), i.ToString()));
                }
                if (page > -1 && page < pagesCount - 1)
                {
                    res.Add(new Tuple<string, string>(AdminPageStrings.NextPage, (page + 1).ToString()));
                }
            }
            return res;
        }

        public ActionResult PagedTasks(int page = 0)
        {
            const int items = 3;
            ViewData["Pager"] = GetPager(items, Logics.TaskAC.Count(), page);
            ViewData["Page"] = page;
            var tasks = Logics.TaskAC.GetPageOrderedByDescending(page, items, u => u.PostDate, u => true);
            return PartialView("PagedTasks", tasks);
        }
    }
}