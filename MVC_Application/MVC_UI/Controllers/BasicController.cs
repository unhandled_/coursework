﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using BL_Interfaces;
using BusinessEntities;
using MVC_UI;

namespace MVC_UI.Controllers
{
    public class BasicController : Controller
    {
        public static object locker = new object();

        private LogicsFactory factory = new LogicsFactory();

        private ILogics logics;

        public ILogics Logics
        {
            get
            {
                lock (locker)
                {
                    string ocKey = "log_" + System.Web.HttpContext.Current.GetHashCode().ToString("x");
                    if (!System.Web.HttpContext.Current.Items.Contains(ocKey))
                    {
                        System.Web.HttpContext.Current.Items.Add(ocKey, factory.GetLogics());
                    }
                    return System.Web.HttpContext.Current.Items[ocKey] as ILogics;
                }
                //return logics ?? (logics = factory.GetLogics());
            }
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.User.Identity.Name))
            {
                User u = Logics.UserAC.FindByLogin(HttpContext.User.Identity.Name, false);
                if (u != null && !u.IsRemoved)
                {
                    Logics.CurrentUser = u;
                }
                else
                {
                    FormsAuthentication.SignOut();
                }
            }
        }
    }
}