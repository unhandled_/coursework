﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ModelsResources;

namespace MVC_UI.Models
{
    public class RegisterModel
    {
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(ModelsStrings))]
        [StringLength(100, MinimumLength = 5, ErrorMessageResourceName = "Length",
            ErrorMessageResourceType = typeof(ModelsStrings))]
        [Remote("ValidateLogin", "Shared")]
        [LocalizedDisplayName("DisplayLogin", NameResourceType = typeof(ModelsStrings))]
        public virtual String Login { get; set; }

        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(ModelsStrings))]
        [RegularExpression(@"^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}\b$", ErrorMessageResourceName = "ValidEmail",
            ErrorMessageResourceType = typeof(ModelsStrings))]
        [LocalizedDisplayName("DisplayEmail", NameResourceType = typeof(ModelsStrings))]
        [Remote("ValidateEmail", "Shared")]
        public virtual String Email { get; set; }

        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(ModelsStrings))]
        [StringLength(100, MinimumLength = 5, ErrorMessageResourceName = "Length",
            ErrorMessageResourceType = typeof(ModelsStrings))]
        [LocalizedDisplayName("DisplayPassword", NameResourceType = typeof(ModelsStrings))]
        public virtual String Password { get; set; }
    }
}