﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using ModelsResources;

namespace MVC_UI.Models
{
    public class AuthenticationModel
    {
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(ModelsStrings))]
        [LocalizedDisplayName("DisplayKey", NameResourceType = typeof(ModelsStrings))]
        public virtual String Key { get; set; }

        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(ModelsStrings))]
        [LocalizedDisplayName("DisplayLogin", NameResourceType = typeof(ModelsStrings))]
        public virtual String Login { get; set; }
    }
}