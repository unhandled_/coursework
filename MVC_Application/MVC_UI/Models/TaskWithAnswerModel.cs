﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using BusinessEntities;
using ModelsResources;

namespace MVC_UI.Models
{
    public class TaskWithAnswerModel : Task
    {
        public readonly Task BaseTask;

        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(ModelsStrings))]
        [LocalizedDisplayName("DisplayAnswer", NameResourceType = typeof(ModelsStrings))]
        public String MyAnswer { get; set; }

        public Int32 LikesCount { get; set; }

        public List<String> LikeLogins { get; set; }

        public Int32 SolvedCount { get; set; }

        public List<String> SolvedLogins { get; set; }

        public sealed override Guid Id { get; set; }

        public TaskWithAnswerModel(Task task)
        {
            this.BaseTask = task;
            Id = task.Id;

            LikesCount = task.Likes.Count;
            LikeLogins = task.Likes.Select(t => t.User.Login).ToList();
            SolvedCount = task.UsersSolved.Count;
            SolvedLogins = task.UsersSolved.Select(t => t.Login).ToList();

        }

        public TaskWithAnswerModel()
        {
            this.BaseTask = new Task();
        }
    }
}