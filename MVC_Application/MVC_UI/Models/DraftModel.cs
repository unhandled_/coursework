﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BusinessEntities;
using ModelsResources;


namespace MVC_UI.Models
{
    public class DraftModel : Draft
    {
        private readonly Draft draft;

        public sealed override Guid Id
        {
            get
            {
                return draft.Id;
            }
            set
            {
                draft.Id = value;
            }
        }

        public Guid OriginalId { get; set; }

        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(ModelsStrings))]
        [Remote("ValidateTaskName", "User", AdditionalFields = "Id,OriginalId")]
        [LocalizedDisplayName("DisplayName", NameResourceType = typeof(ModelsStrings))]
        public override String Name
        {
            get
            {
                return draft.Name;
            }
            set
            {
                draft.Name = value;
            }
        }

        public override string Description
        {
            get
            {
                return draft.Description;
            }
            set
            {
                draft.Description = value;
            }
        }

        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(ModelsStrings))]
        [LocalizedDisplayName("DisplayText", NameResourceType = typeof(ModelsStrings))]
        public override String Text
        {
            get
            {
                return draft.Text;
            }
            set
            {
                draft.Text = value;
            }
        }

        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(ModelsStrings))]
        [LocalizedDisplayName("DisplayTechnology", NameResourceType = typeof(ModelsStrings))]
        public virtual String TechnologyName { get; set; }

        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(ModelsStrings))]
        [LocalizedDisplayName("DisplayLanguage", NameResourceType = typeof(ModelsStrings))]
        public virtual String LocaleLanguage { get; set; }

        [Remote("ValidateTag", "User", AdditionalFields = "Id")]
        public virtual String TagName { get; set; }

        public virtual List<String> TagList { get; set; }

        [Remote("ValidateAnswer", "User", AdditionalFields = "Id")]
        public virtual String AnswerName { get; set; }

        public virtual List<String> AnswerList { get; set; }

        public DraftModel(Draft draft, String tagName, String answerName)
        {
            this.draft = draft;
            OriginalId = draft.Original != null ? draft.Original.Id : Guid.Empty;

            TechnologyName = draft.Technology != null ? draft.Technology.Name : null;
            TagName = tagName;
            AnswerName = answerName;

            TagList = draft.Tags.Select(t => t.Text).ToList();
            AnswerList = draft.Answers.Select(t => t.Text).ToList();
        }

        public DraftModel() 
        {
            this.draft = new Draft();
            TagList = new List<String>();
            AnswerList = new List<String>();
        }
    }
}