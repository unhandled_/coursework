﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using ModelsResources;

namespace MVC_UI.Models
{
    public class RemovedUserModel
    {
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(ModelsStrings))]
        [LocalizedDisplayName("DisplayName", NameResourceType = typeof(ModelsStrings))]
        public String Login { get; set; }

        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(ModelsStrings))]
        [LocalizedDisplayName("DisplayPassword", NameResourceType = typeof(ModelsStrings))]
        public String Password { get; set; }
    }
}