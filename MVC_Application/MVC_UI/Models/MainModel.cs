﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BusinessEntities;

namespace MVC_UI.Models
{
    public class MainModel
    {
        public List<Task> RecentTasks;

        public MainModel()
        {
            RecentTasks = new List<Task>();
        }
    }
}