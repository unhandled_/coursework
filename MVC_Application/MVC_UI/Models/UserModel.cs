﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BusinessEntities;
using ModelsResources;

namespace MVC_UI.Models
{
    public class UserModel : User
    {
        private User user;

        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(ModelsStrings))]
        [StringLength(100, MinimumLength = 5, ErrorMessageResourceName = "Length",
            ErrorMessageResourceType = typeof(ModelsStrings))]
        [LocalizedDisplayName("DisplayLogin", NameResourceType = typeof(ModelsStrings))]
        [Remote("ValidateLogin", "Admin", AdditionalFields = "UserId")]
        public override String Login
        {
            get
            {
                return user.Login;
            }
            set
            {
                user.Login = value;
            }
        }

        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(ModelsStrings))]
        [RegularExpression(@"^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}\b$", ErrorMessageResourceName = "ValidEmail",
            ErrorMessageResourceType = typeof(ModelsStrings))]
        [Remote("ValidateEmail", "Admin", AdditionalFields = "UserId")]
        [LocalizedDisplayName("DisplayEmail", NameResourceType = typeof(ModelsStrings))]
        public override string Email
        {
            get
            {
                return user.Email;
            }
            set
            {
                user.Email = value;
            }
        }

        [DataType(DataType.Password, ErrorMessageResourceName = "Data_Type",
            ErrorMessageResourceType = typeof(ModelsStrings))]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(ModelsStrings))]
        [StringLength(100, MinimumLength = 5, ErrorMessageResourceName = "Length",
            ErrorMessageResourceType = typeof(ModelsStrings))]
        [LocalizedDisplayName("DisplayPassword", NameResourceType = typeof(ModelsStrings))]
        public override string Password
        {
            get
            {
                return user.Password;
            }
            set
            {
                user.Password = value;
            }
        }

        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(ModelsStrings))]
        [Remote("ValidateLanguage", "Admin")]
        [LocalizedDisplayName("DisplayLanguage", NameResourceType = typeof(ModelsStrings))]
        public string LanguageLocale { get; set; }

        public List<SelectListItem> Languages { get; set; }

        public override Guid UserId
        {
            get { return user.UserId; }
            set { user.UserId = value; }
        }

        public UserModel(User user)
        {
            this.user = user;
        }

        public UserModel()
        {
            user = new User();
        }
    }
}