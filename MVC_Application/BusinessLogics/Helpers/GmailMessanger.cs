using System;
using System.Configuration;
using System.Net;
using System.Net.Mail;
using BL_Interfaces.Exceptions;
using BusinessEntities;
using BusinessLogics.Properties;

namespace BusinessLogics.Helpers
{
    /// <summary>
    /// Class for sending letters by Gmail smtp.
    /// </summary>
    public static class EmailConfirmationSender
    {
        private static readonly SmtpClient client;

        static EmailConfirmationSender()
        {
            client = new SmtpClient
                {
                    Host = Settings.Default.MailSenderHost,
                    Port = Settings.Default.MailSenderPort,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(Settings.Default.MailSenderLogin,
                                                        Settings.Default.MailSenderPassword)
                };
        }

        /// <summary>
        /// Sends the message.
        /// </summary>
        /// <param name="user">User.</param>
        /// <param name="body">Body of letter.</param>
        /// <param name="subject">Subject of letter.</param>
        /// <exception cref="VerificationSendException">Thrown if errors occured while sending letter with verification.</exception>
        public static void SendMessage(User user, String body, String subject)
        {
            using (MailMessage message = GetMessage(user, body, subject))
            {
                try
                {
                    client.Send(message);
                }
                catch (Exception e)
                {
                    throw new VerificationSendException(e);
                }
            }
        }

        private static MailMessage GetMessage(User user, String body, String subject)
        {
            return new MailMessage(Settings.Default.MailSenderLogin, user.Email)
                {
                    Subject = subject,
                    Body = body,
                };
        }
    }
}