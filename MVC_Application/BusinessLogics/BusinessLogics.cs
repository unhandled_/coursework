﻿using System;
using System.Web.Security;
using BL_Interfaces;
using BL_Interfaces.AccessComponents.Concrete;
using BusinessEntities;
using BusinessLogics.Components;
using BusinessLogics.Properties;
using DAL_Interfaces;
using MembershipRoutines;

namespace BusinessLogics
{
    /// <summary>
    /// Concrete implementation of ILogics interface.
    /// </summary>
    public class BusinessLogics: ILogics
    {
        /// <summary>
        /// Currently logged in user.
        /// </summary>
        public User CurrentUser { get; set; }

        /// <summary>
        /// Database that is used in current logics.
        /// </summary>
        public IDatabase Database { get; set; }

        /// <summary>
        /// Answer access component.
        /// </summary>
        public IAnswerAccessComponent AnswerAC { get; set; }

        /// <summary>
        /// Draft access component.
        /// </summary>
        public IDraftAccessComponent DraftAC { get; set; }

        /// <summary>
        /// Language access component.
        /// </summary>
        public ILanguageAccessComponent LanguageAC { get; set; }

        /// <summary>
        /// Tag access component.
        /// </summary>
        public ITagAccessComponent TagAC { get; set; }

        /// <summary>
        /// Task access component.
        /// </summary>
        public ITaskAccessComponent TaskAC { get; set; }

        /// <summary>
        /// Technology access component.
        /// </summary>
        public ITechnologyAccessComponent TechnologyAC { get; set; }

        /// <summary>
        /// User access component.
        /// </summary>
        public IUserAccessComponent UserAC { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="BusinessLogics" /> class.
        /// </summary>
        /// <param name="database">The database.</param>
        public BusinessLogics(IDatabase database)
        {
            Database = database;
            InitializeLogics();
        }

        private void InitializeLogics()
        {
            InitializeAccessComponents();
            ConfigureMembershipProviders();
            InitializeDefaultRoles();
            InitializeDefaultLanguages();
            InitializeAdministratorUser();
            DoCleanUp();
        }

        private void DoCleanUp()
        {
            if (Settings.Default.DoCleanUpEveryRun)
            {
                TagAC.CleanUp();
                TechnologyAC.CleanUp();
            }
        }

        private void InitializeAdministratorUser()
        {
            if (Settings.Default.CreateAdministratorUser
                && Membership.GetUser(Settings.Default.AdministratorUserLogin) == null)
            {
                MembershipCreateStatus status;
                Membership.CreateUser(Settings.Default.AdministratorUserLogin,
                                      Settings.Default.AdministratorUserPassword,
                                      Settings.Default.AdministratorUserEmail,
                                      null, null, true, null, out status);
                Roles.AddUserToRole(Settings.Default.AdministratorUserLogin,
                                    Settings.Default.AdministratorRoleName);
            }
        }

        private void ConfigureMembershipProviders()
        {
            ((CodeFirstMembershipProvider)Membership.Provider).Database = Database;
            ((CodeFirstRoleProvider)Roles.Provider).Database = Database;
        }

        private void InitializeDefaultLanguages()
        {
            InitializeRussianLanguage();
            InitializeEnglishLanguage();
        }

        private void InitializeEnglishLanguage()
        {
            InitializeLanguage("en", "English");
        }

        private void InitializeRussianLanguage()
        {
            InitializeLanguage("ru", "Русский");
        }

        private void InitializeLanguage(String locale, String name)
        {
            if (LanguageAC.Find(locale, false) == null)
            {
                LanguageAC.Create(locale, name);
            }
        }

        private void InitializeDefaultRoles()
        {
            InitializeAdminRole();
            InitializeUserRole();
        }

        private void InitializeAccessComponents()
        {
            UserAC = new UserAccessComponent(this);
            TagAC = new TagAccessComponent(this);
            TechnologyAC = new TechnologyAccessComponent(this);
            LanguageAC = new LanguageAccessComponent(this);
            TaskAC = new TaskAccessComponent(this);
            AnswerAC = new AnswerAccessComponent(this);
            DraftAC = new DraftAccessComponent(this);
        }

        private void InitializeUserRole()
        {
            InitializeRole(Settings.Default.UserRoleName);
        }

        private void InitializeAdminRole()
        {
            InitializeRole(Settings.Default.AdministratorRoleName);
        }

        private void InitializeRole(String roleName)
        {
            if (!Roles.RoleExists(roleName))
            {
                Roles.CreateRole(roleName);
            }
        }

        public void Dispose()
        {
            Database.Dispose();
        }
    }
}