﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Security;
using BL_Interfaces;
using BL_Interfaces.Enumerations;
using BL_Interfaces.Exceptions;

namespace BusinessLogics.Components
{
    /// <summary>
    /// Basic class for Access components.
    /// </summary>
    /// <typeparam name="ItemType">The type of items, processed by access component.</typeparam>
    public class BasicAccessComponent<ItemType>
    {
        /// <summary>
        /// Gets or sets the logics.
        /// </summary>
        /// <value>
        /// The logics.
        /// </value>
        public ILogics Logics { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="BasicAccessComponent{ItemType}" /> class.
        /// </summary>
        /// <param name="logics">The logics.</param>
        public BasicAccessComponent(ILogics logics)
        {
            Logics = logics;
        }

        /// <summary>
        /// Checks the logics and database for null.
        /// </summary>
        /// <exception cref="BL_Interfaces.Exceptions.DatabaseFailedException">Exception is thrown when logics or database are null.</exception>
        protected void CheckLogics()
        {
            if (Logics == null || Logics.Database == null)
            {
                throw new DatabaseFailedException(null);
            }
        }

        /// <summary>
        /// Check whether user meets requirements for access to operation.
        /// </summary>
        /// <param name="access">Determines requirements for user.</param>
        /// <param name="ownerLogin">Login of user that is owner of some content.</param>
        /// <param name="role">Role that user should have.</param>
        /// <exception cref="NoRightsException">Thrown if user can not perform some action.</exception>
        protected void CheckUser(OperationAccess access, String ownerLogin = null, String role = null)
        {
            CheckLogics();
            CheckForAuthorization(access);
            if (AccessEnumerationHasFlag(access, OperationAccess.Owner) && ownerLogin == Logics.CurrentUser.Login)
            {
                return;
            }
            CheckForSomeRole(access, role);
        }

        /// <summary>
        /// Finds item with specified id.
        /// </summary>
        /// <param name="id">Unique identifier of item to be found.</param>
        /// <returns>Item of null.</returns>
        public ItemType Find(Guid id)
        {
            CheckLogics();
            return Logics.Database.Repository<ItemType>().Find(id);
        }

        /// <summary>
        /// Filters sequence with the specified selector.
        /// </summary>
        /// <param name="selector">The selector.</param>
        /// <returns>Filtered sequence.</returns>
        public IEnumerable<ItemType> Where(Func<ItemType, Boolean> selector)
        {
            CheckLogics();
            return Logics.Database.Repository<ItemType>().Where(selector);
        }

        /// <summary>
        /// Gets all sequence.
        /// </summary>
        /// <returns>Sequence containing all items.</returns>
        public IEnumerable<ItemType> All()
        {
            CheckLogics();
            return Logics.Database.Repository<ItemType>().All();
        }

        /// <summary>
        /// Gets count of items in sequence.
        /// </summary>
        /// <returns>Number of items.</returns>
        public int Count()
        {
            CheckLogics();
            return Logics.Database.Repository<ItemType>().Count();
        }

        /// <summary>
        /// Gets count of items in filtered sequence.
        /// </summary>
        /// <param name="selector">The selector.</param>
        /// <returns>Number of items in filtered sequence.</returns>
        public int Count(Func<ItemType, Boolean> selector)
        {
            CheckLogics();
            return Logics.Database.Repository<ItemType>().Count(selector);
        }

        /// <summary>
        /// Returns first or default item from sequence.
        /// </summary>
        /// <param name="selector">The selector.</param>
        /// <returns>First item or null.</returns>
        public ItemType FirstOrDefault(Func<ItemType, Boolean> selector)
        {
            CheckLogics();
            return Logics.Database.Repository<ItemType>().FirstOrDefault(selector);
        }

        /// <summary>
        /// Gets the page.
        /// </summary>
        /// <param name="pageNum">Number of current page.</param>
        /// <param name="itemsPerPage">Number of items per single page.</param>
        /// <param name="selector">The selector.</param>
        /// <returns>Sequence for current page.</returns>
        public IEnumerable<ItemType> GetPage(int pageNum,
                                             int itemsPerPage,
                                             Func<ItemType, Boolean> selector)
        {
            CheckLogics();
            return Logics.Database.Repository<ItemType>()
                .Where(selector)
                .Skip(pageNum * itemsPerPage)
                .Take(itemsPerPage);
        }

        /// <summary>
        /// Gets the page with items ordered by descending.
        /// </summary>
        /// <typeparam name="T">Field to be sorted by.</typeparam>
        /// <param name="pageNum">Number of current page.</param>
        /// <param name="itemsPerPage">Number of items per single page.</param>
        /// <param name="selector">The selector.</param>
        /// <returns>Sequence for current page.</returns>
        /// <param name="orderbyFunc">The orderby func.</param>
        /// <returns>Sequence for current page ordered by some filed by descending.</returns>
        public IEnumerable<ItemType> GetPageOrderedByDescending<T>(int pageNum,
                                                                   int itemsPerPage,
                                                                   Func<ItemType, T> orderbyFunc,
                                                                   Func<ItemType, Boolean> selector)
        {
            CheckLogics();
            return Logics.Database.Repository<ItemType>()
                .Where(selector)
                .Skip(pageNum * itemsPerPage)
                .Take(itemsPerPage)
                .OrderByDescending(orderbyFunc);
        }

        /// <summary>
        /// Gets the page with items ordered by ascending.
        /// </summary>
        /// <typeparam name="T">Field to be sorted by.</typeparam>
        /// <param name="pageNum">Number of current page.</param>
        /// <param name="itemsPerPage">Number of items per single page.</param>
        /// <param name="selector">The selector.</param>
        /// <returns>Sequence for current page.</returns>
        /// <param name="orderbyFunc">The orderby func.</param>
        /// <returns>Sequence for current page ordered by some filed by ascending.</returns>
        public IEnumerable<ItemType> GetPageOrderedByAscending<T>(int pageNum,
                                                                  int itemsPerPage,
                                                                  Func<ItemType, T> orderbyFunc,
                                                                  Func<ItemType, Boolean> selector)
        {
            CheckLogics();
            return Logics.Database.Repository<ItemType>()
                .Where(selector)
                .Skip(pageNum * itemsPerPage)
                .Take(itemsPerPage)
                .OrderByDescending(orderbyFunc);
        }


        /// <summary>
        /// Strict search of item. It means if search result equals to exception state, then ExceptionToThrow will be thrown.
        /// </summary>
        /// <typeparam name="ExceptionToThrow">The type of the exception to throw.</typeparam>
        /// <param name="id">The id.</param>
        /// <param name="exceptionState">State of the exception.</param>
        /// <returns></returns>
        /// <exception cref="ExceptionToThrow">Type of exception to be thrown.</exception>
        public ItemType FindStrict<ExceptionToThrow>(Guid id, SearchState exceptionState)
            where ExceptionToThrow : Exception, new()
        {
            CheckLogics();
            ItemType item = Logics.Database.Repository<ItemType>().Find(id);
            ThrowExceptionIfNeeded<ExceptionToThrow>(exceptionState, item);
            return item;
        }

        private static void ThrowExceptionIfNeeded<ExceptionToThrow>(SearchState exceptionState, ItemType item) where ExceptionToThrow : Exception, new()
        {
            if ((exceptionState == SearchState.ItemFound && (item != null))
                || (exceptionState == SearchState.ItemNotFound && item == null))
            {
                throw new ExceptionToThrow();
            }
        }

        private void CheckForSomeRole(OperationAccess access, String role)
        {
            if (AccessEnumerationHasFlag(access, OperationAccess.AlternativeRole)
                && Logics.CurrentUser.Roles.All(t => t.RoleName != role))
            {
                throw new NoRightsException();
            }
        }

        private void CheckForAuthorization(OperationAccess access)
        {
            if ((access & OperationAccess.Authorized) == OperationAccess.Authorized
                && Logics.CurrentUser == null)
            {
                throw new NoRightsException();
            }
        }

        private Boolean AccessEnumerationHasFlag(OperationAccess access, OperationAccess flag)
        {
            return (access & flag) == flag;
        }
    }
}