﻿using System;
using BL_Interfaces;
using BL_Interfaces.AccessComponents.Concrete;
using BL_Interfaces.Enumerations;
using BL_Interfaces.Exceptions;
using BusinessEntities;
using BusinessLogics.Properties;

namespace BusinessLogics.Components
{
    /// <summary>
    /// Class for access to operations with language's logics.
    /// </summary>
    public class LanguageAccessComponent : BasicAccessComponent<Language>, ILanguageAccessComponent
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LanguageAccessComponent" /> class.
        /// </summary>
        /// <param name="logics">The logics.</param>
        public LanguageAccessComponent(ILogics logics)
            : base(logics)
        {
        }

        /// <summary>
        /// Create language in database.
        /// </summary>
        /// <param name="locale">Locale name of this language.</param>
        /// <param name="name">Name of this language.</param>
        /// <returns>
        /// Created language.
        /// </returns>
        /// <exception cref="DatabaseFailedException">Thrown if there are errors in database.</exception> 
        /// <exception cref="LanguageAlreadyExistsException">Thrown if locale name already exists in database.</exception>
        public Language Create(String locale, String name)
        {
            FindStrictByLocale<LanguageAlreadyExistsException>(locale, SearchState.ItemFound);
            Language language = new Language
                {
                    Id = Guid.NewGuid(),
                    Locale = locale,
                    Name = name,
                };
            Logics.Database.LanguageRepository.Add(language);
            return language;
        }

        /// <summary>
        /// Edit language.
        /// </summary>
        /// <param name="languageId">Language's unique identifier.</param>
        /// <param name="newLocale">New locale name of language.</param>
        /// <param name="newName">New name of language.</param>
        /// <exception cref="LanguageAlreadyExistsException">Thrown if locale name already exists in database.</exception>
        /// <exception cref="DatabaseFailedException">Thrown if there are errors in database.</exception>
        /// <exception cref="LanguageNotFoundException">Thrown if language was not found in database.</exception>
        public void Edit(Guid languageId, String newLocale, String newName)
        {
            CheckUser(OperationAccess.AlternativeRole, null, Settings.Default.AdministratorRoleName);
            Language language = FindStrict<LanguageNotFoundException>(languageId, SearchState.ItemNotFound);
            FindStrictByLocale<LanguageAlreadyExistsException>(newLocale, SearchState.ItemFound);
            language.Locale = newLocale;
            language.Name = newName;
            Logics.Database.LanguageRepository.AddOrUpdate(language);
        }

        /// <summary>
        /// Remove language with some unique identifier.
        /// </summary>
        /// <param name="locale">Locale name of language to be removed.</param>
        /// <exception cref="ExistingDependencyException">Exception is thrown when language to be removed has existing dependencies in database.</exception>
        /// <exception cref="DatabaseFailedException">Thrown if there are errors in database.</exception>
        /// <exception cref="LanguageNotFoundException">Thrown if language was not found in database.</exception>
        /// <exception cref="NoRightsException">Thrown if user have no rights to perform current action.</exception>
        public void Remove(String locale)
        {
            CheckUser(OperationAccess.AlternativeRole, null, Settings.Default.AdministratorRoleName);
            Language language = Find(locale, true);
            if (language.Drafts.Count == 0 && language.Users.Count == 0 && language.Tasks.Count == 0)
            {
                Logics.Database.LanguageRepository.Remove(language);
            }
            else
            {
                throw new ExistingDependencyException();
            }
        }

        /// <summary>
        /// Find language with some locale name.
        /// </summary>
        /// <param name="locale">Locale name of language to be found.</param>
        /// <param name="throwExceptionIfNotFound">If true and language was not found in database - LanguageNotFoundException is thrown.</param>
        /// <returns></returns>
        /// <exception cref="DatabaseFailedException">Thrown if there are errors in database.</exception>
        /// <exception cref="LanguageNotFoundException">Thrown if language was not found in database.</exception>
        public Language Find(String locale, Boolean throwExceptionIfNotFound)
        {
            CheckLogics();
            Language language = Logics.Database.LanguageRepository
                .FirstOrDefault(l => l.Name == locale || l.Locale == locale);
            if (language == null && throwExceptionIfNotFound)
            {
                throw new LanguageNotFoundException();
            }
            return language;
        }

        internal Language FindStrictByLocale<ExceptionToThrow>(String locale, SearchState exceptionState)
            where ExceptionToThrow : Exception, new()
        {
            CheckLogics();
            Language language = Logics.Database.LanguageRepository.Find(locale);
            ThrowExceptionIfNeeded<ExceptionToThrow>(exceptionState, language);
            return language;
        }

        private static void ThrowExceptionIfNeeded<ExceptionToThrow>(SearchState exceptionState,
                                                                     Language language)
            where ExceptionToThrow : Exception, new()
        {
            if ((exceptionState == SearchState.ItemFound && (language != null))
                || (exceptionState == SearchState.ItemNotFound && language == null))
            {
                throw new ExceptionToThrow();
            }
        }
    }
}