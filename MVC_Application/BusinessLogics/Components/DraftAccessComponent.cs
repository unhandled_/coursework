﻿using System;
using System.Collections.Generic;
using System.Linq;
using BL_Interfaces;
using BL_Interfaces.AccessComponents.Concrete;
using BL_Interfaces.Enumerations;
using BL_Interfaces.Exceptions;
using BusinessEntities;
using BusinessLogics.Properties;

namespace BusinessLogics.Components
{
    /// <summary>
    /// Class for access to operations with drafts's logics.
    /// </summary>
    public class DraftAccessComponent : BasicAccessComponent<Draft>, IDraftAccessComponent
    {
        /// <summary>
        /// Removes the tag from draft.
        /// </summary>
        /// <param name="draftId">The draft id.</param>
        /// <param name="tagText">The tag text.</param>
        /// <exception cref="BL_Interfaces.Exceptions.TagNotFoundException"></exception>
        public void RemoveTag(Guid draftId, String tagText)
        {
            var draft = FindStrict<DraftNotFoundException>(draftId, SearchState.ItemNotFound);
            var tagToRemove = draft.Tags.FirstOrDefault(tag => tag.Text == tagText);
            if (tagToRemove == null)
            {
                throw new TagNotFoundException();
            }
            draft.Tags.Remove(tagToRemove);
            Logics.Database.DraftRepository.AddOrUpdate(draft);
        }

        /// <summary>
        /// Removes the answer from draft.
        /// </summary>
        /// <param name="draftId">The draft id.</param>
        /// <param name="answerText">The answer text.</param>
        /// <exception cref="BL_Interfaces.Exceptions.AnswerNotFoundException"></exception>
        public void RemoveAnswer(Guid draftId, String answerText)
        {
            var draft = FindStrict<DraftNotFoundException>(draftId, SearchState.ItemNotFound);
            var answerToRemove = draft.Answers.FirstOrDefault(answer => answer.Text.Trim() == answerText.Trim());
            if (answerToRemove == null)
            {
                throw new AnswerNotFoundException();
            }
            draft.Answers.Remove(answerToRemove);
            Logics.Database.DraftRepository.AddOrUpdate(draft);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DraftAccessComponent" /> class.
        /// </summary>
        /// <param name="logics">The logics.</param>
        public DraftAccessComponent(ILogics logics)
            : base(logics)
        {
        }

        /// <summary>
        /// Create draft in database from original task.
        /// </summary>
        /// <param name="originalId">Unique identifier of original task.</param>
        /// <returns>
        /// Created draft.
        /// </returns>
        /// <exception cref="TaskNotFoundException">Thrown in task with specified unique identifier was not found in database.</exception>
        /// <exception cref="NoRightsException">Thrown if user have to rights to perform current action.</exception>
        /// <exception cref="DatabaseFailedException">Thrown if there are errors in database.</exception>
        /// <exception cref="DraftAlreadyExistsException">Thrown if draft for specified task already exists.</exception>
        public Draft Create(Guid originalId)
        {
            Task original = Logics.TaskAC.FindStrict<TaskNotFoundException>(originalId, SearchState.ItemNotFound);
            CheckUser(OperationAccess.Owner, original.Author.Login);
            CheckIfDraftAlreadyExists(original);
            Draft draft = GetDraft(original);
            Logics.Database.DraftRepository.Add(draft);
            return draft;
        }

        /// <summary>
        /// Create new draft in database.
        /// </summary>
        /// <param name="description">Description of draft.</param>
        /// <param name="text">Text of draft.</param>
        /// <param name="name">Name of draft.</param>
        /// <param name="technology">Technology that is used in this draft.</param>
        /// <param name="locale">Language of draft.</param>
        /// <param name="tags">List of tags for this draft.</param>
        /// <param name="answers">List of answers for this draft.</param>
        /// <returns>
        /// Created draft.
        /// </returns>
        /// <exception cref="NoRightsException">Thrown if user have to rights to perform current action.</exception>
        /// <exception cref="DatabaseFailedException">Thrown if there are errors in database.</exception>
        /// <exception cref="LanguageNotFoundException">Thrown if language was not found in database.</exception>
        public Draft Create(String description, String text,
                            String name, String technology,
                            String locale, List<String> tags,
                            List<String> answers)
        {
            CheckUser(OperationAccess.Authorized);
            Draft draft = new Draft
            {
                Id = Guid.NewGuid(),
                Author = Logics.CurrentUser,
                Language = Logics.LanguageAC.Find(locale, true),
                LastModified = DateTime.UtcNow,
                Name = name,
                Original = null,
                Technology = Logics.TechnologyAC.GetWithName(technology),
                Text = text,
                Tags = tags.Select(Logics.TagAC.GetWithName).ToList(),
                Answers = answers.Select(Logics.AnswerAC.Create).ToList(),
                Description = description,
            };
            Logics.Database.DraftRepository.Add(draft);
            return draft;
        }

        /// <summary>
        /// Edit draft.
        /// </summary>
        /// <param name="draftId">Draft's to be edited unique identifier.</param>
        /// <param name="newDescription">New draft's description.</param>
        /// <param name="newText">New draft's text.</param>
        /// <param name="newName">New draft's name.</param>
        /// <param name="technology">New draft's technology.</param>
        /// <param name="locale">New language of draft.</param>
        /// <param name="newTags">List of new tags.</param>
        /// <param name="newAnswers">List of new answers.</param>
        /// <exception cref="NoRightsException">Thrown if user have to rights to perform current action.</exception>
        /// <exception cref="DatabaseFailedException">Thrown if there are errors in database.</exception>
        /// <exception cref="DraftAlreadyExistsException">Thrown if draft for specified task already exists.</exception>
        /// <exception cref="LanguageNotFoundException">Thrown if language was not found in database.</exception>
        public void Edit(Guid draftId, String newDescription,
                         String newText, String newName,
                         String technology, String locale,
                         List<String> newTags, List<String> newAnswers)
        {
            Draft draft = FindStrict<DraftNotFoundException>(draftId, SearchState.ItemNotFound);
            CheckUser(OperationAccess.Owner, draft.Author.Login);
            ClearOldDraftData(draft);
            draft.LastModified = DateTime.UtcNow;
            draft.Name = newName ?? String.Empty;
            draft.Description = newDescription ?? String.Empty;
            draft.Text = newText ?? String.Empty;
            draft.Technology = technology == null ? null : Logics.TechnologyAC.GetWithName(technology);
            draft.Language = locale == null ? null : Logics.LanguageAC.Find(locale, true);
            draft.Tags = newTags == null ? new List<Tag>() : newTags.Select(Logics.TagAC.GetWithName).ToList();
            draft.Answers = newAnswers == null ? new List<Answer>() : newAnswers.Select(Logics.AnswerAC.Create).ToList();
            Logics.Database.DraftRepository.AddOrUpdate(draft);
        }


        /// <summary>
        /// Save draft as task. If no original - new task created, otherwise original will be rewritten.
        /// </summary>
        /// <param name="draftId">Draft's unique identifier.</param>
        /// <exception cref="NoRightsException">Thrown if user have to rights to perform current action.</exception>
        /// <exception cref="DatabaseFailedException">Thrown if there are errors in database.</exception>
        /// <exception cref="DraftAlreadyExistsException">Thrown if draft for specified task already exists.</exception>
        /// <exception cref="TaskAlreadyExistsException">Thrown if task with NameForLink property already existst in database.</exception>
        public void SaveChanges(Guid draftId)
        {
            Draft draft = FindStrict<DraftNotFoundException>(draftId, SearchState.ItemNotFound);
            CheckUser(OperationAccess.Owner, draft.Author.Login);
            ApplyChangesToTask(draft);
            Remove(draft.Id);
        }

        /// <summary>
        /// Remove draft from database.
        /// </summary>
        /// <param name="draftId">Draft's unique identifier.</param>
        /// <exception cref="DraftNotFoundException">Throw if draft with specified unique identifier was not found in database.</exception>
        public void Remove(Guid draftId)
        {
            Draft draft = FindStrict<DraftNotFoundException>(draftId, SearchState.ItemNotFound);
            CheckUser(OperationAccess.Owner, draft.Author.Login);
            RemoveAllDraftData(draft);
        }

        private void ClearOldDraftData(Draft draft)
        {
            RemoveAnswersOfOriginal(draft);
            draft.Tags.Clear();
            Logics.Database.DraftRepository.AddOrUpdate(draft);
        }
        
        private IList<Tag> GetTags(Task original)
        {
            return original.Tags.Select(t => t).ToList();
        }

        private IList<Answer> GetAnswers(Task original)
        {
            return original.Answers.Select(a => Logics.AnswerAC.Create(a.Text)).ToList();
        }

        private Draft GetDraft(Task original)
        {
            return new Draft()
            {
                Id = Guid.NewGuid(),
                Author = Logics.CurrentUser,
                Language = original.Language,
                LastModified = DateTime.UtcNow,
                Name = original.Name,
                Original = original,
                Text = original.Text,
                Technology = original.Technology,
                Tags = GetTags(original),
                Answers = GetAnswers(original),
                Description = original.Description,
            };
        }

        private void CheckIfDraftAlreadyExists(Task original)
        {
            if (Logics.DraftAC.FirstOrDefault(d => d.Original != null && d.Original.Id == original.Id) != null)
            {
                throw new DraftAlreadyExistsException();
            }
        }

        private void RemoveAllDraftData(Draft draft)
        {
            draft.Tags.Clear();
            RemoveAnswersOfOriginal(draft);
            Logics.Database.DraftRepository.AddOrUpdate(draft);
            Logics.Database.DraftRepository.Remove(draft);
        }

        private void RemoveAnswersOfOriginal(Draft task)
        {
            List<Answer> answers = task.Answers.ToList();
            for (int i = 0; i < answers.Count; i++)
            {
                Logics.Database.AnswerRepository.Remove(answers[i]);
            }
            task.Answers.Clear();
        }


        private void ApplyChangesToTask(Draft draft)
        {
            if (draft.Original == null)
            {
                CreateNewTaskFromOriginal(draft);
            }
            else
            {
                EditTaskFromOriginal(draft);
            }
        }

        private void EditTaskFromOriginal(Draft draft)
        {
            Task original = draft.Original;
            original.Text = draft.Text;
            original.PostDate = draft.LastModified;
            original.Language = draft.Language;
            original.Name = draft.Name;
            var originalNameForLink = original.NameForLink;
            var taskWithNameForLink = Logics.TaskAC.FirstOrDefault(task => task.NameForLink == originalNameForLink);
            if (taskWithNameForLink != null)
            {
                if (taskWithNameForLink.Id != original.Id)
                {
                    throw new TaskAlreadyExistsException();
                }
            }
            original.Author = draft.Author;
            original.Description = draft.Description;
            original.Technology = draft.Technology;
            original.Answers = draft.Answers.Select(answer => answer).ToList();
            original.Tags = draft.Tags.Select(tag => tag).ToList();
            Logics.Database.TaskRepository.AddOrUpdate(original);
        }

        private void CreateNewTaskFromOriginal(Draft draft)
        {
            Logics.TaskAC.Create(draft.Description, draft.Text, draft.Name,
                                 draft.Technology.Name, draft.Language.Locale,
                                 draft.Tags.Select(tag => tag.Text).ToList(),
                                 draft.Answers.Select(answer => answer.Text).ToList());
        }
    }
}