﻿using System;
using System.Collections.Generic;
using System.Linq;
using BL_Interfaces;
using BL_Interfaces.AccessComponents.Concrete;
using BL_Interfaces.Enumerations;
using BL_Interfaces.Exceptions;
using BusinessEntities;

namespace BusinessLogics.Components
{
    /// <summary>
    /// Class for access to operations with task's logics.
    /// </summary>
    public class TaskAccessComponent : BasicAccessComponent<Task>, ITaskAccessComponent
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TaskAccessComponent" /> class.
        /// </summary>
        /// <param name="logics">The logics.</param>
        public TaskAccessComponent(ILogics logics)
            : base(logics)
        {
        }

        /// <summary>
        /// Finds tasks by specified query.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <returns>
        /// Sequence of tasks.
        /// </returns>
        /// <exception cref="DatabaseFailedException">Thrown if there are errors in database.</exception>
        public IEnumerable<Task> Find(string query)
        {
            return String.IsNullOrEmpty(query)
                       ? Enumerable.Empty<Task>()
                       : Logics.Database.TaskRepository.Find(query);
        }

        /// <summary>
        /// Create new task in database.
        /// </summary>
        /// <param name="description">Description of task.</param>
        /// <param name="text">Text of task.</param>
        /// <param name="name">Name of task.</param>
        /// <param name="technology">Technology that is used in this task.</param>
        /// <param name="locale">Language of task.</param>
        /// <param name="tags">List of tags for this task.</param>
        /// <param name="answers">List of answers for this task.</param>
        /// <returns>
        /// Created task.
        /// </returns>
        /// <exception cref="TaskAlreadyExistsException">Thrown if task with inputted name already exists in database.</exception>
        /// <exception cref="DatabaseFailedException">Thrown if there are errors in database.</exception>
        /// <exception cref="LanguageNotFoundException">Thrown if there is not language in database with inputted locale.</exception>
        /// <exception cref="NoRightsException">Thrown if user have no rights to perform current action.</exception>
        public Task Create(String description, String text,
                           String name, String technology,
                           String locale, List<String> tags,
                           List<String> answers)
        {
            CheckUser(OperationAccess.Authorized);
            Task task = GetTask(description, text, name, technology, locale, tags, answers);
            CheckIfTaskAlreadyExists(task);
            AddTask(task);
            return task;
        }

        private void AddTask(Task task)
        {
            Logics.CurrentUser.Tasks.Add(task);
            Logics.Database.TaskRepository.Add(task);
        }

        /// <summary>
        /// Try to solve task by current user.
        /// </summary>
        /// <param name="taskId">Unique identifier of task to be solved.</param>
        /// <param name="answer">Text of estimated answer.</param>
        /// <returns>
        /// True if task was solved successfully, otherwise false.
        /// </returns>
        /// <exception cref="TaskNotFoundException">Thrown if task was not found in database.</exception>
        /// <exception cref="TaskAlreadySolvedException">Thrown if task is already solved by current user.</exception>
        /// <exception cref="DatabaseFailedException">Thrown if task are errors in database.</exception>
        /// <exception cref="NoRightsException">Thrown if user have no rights to perform current action.</exception>
        public Boolean TrySolve(Guid taskId, String answer)
        {
            CheckUser(OperationAccess.Authorized);
            Task task = FindStrict<TaskNotFoundException>(taskId, SearchState.ItemNotFound);
            CheckIfTaskIsAlreadySolved(task);
            return AddIfAnswerIsCorrect(task, answer);
        }

        /// <summary>
        /// Current user pressed Like button of task.
        /// </summary>
        /// <param name="taskId">Unique identifier of task to be liked.</param>
        /// <returns>
        /// New number of likes.
        /// </returns>
        /// <exception cref="TaskNotFoundException">Thrown if task was not found in database.</exception>
        /// <exception cref="DatabaseFailedException">Thrown if task are errors in database.</exception>
        /// <exception cref="NoRightsException">Thrown if user have no rights to perform current action.</exception>
        public Int32 Like(Guid taskId)
        {
            CheckUser(OperationAccess.Authorized);
            Task task = FindStrict<TaskNotFoundException>(taskId, SearchState.ItemNotFound);
            Like userLike = FindLike(taskId, Logics.CurrentUser.Login);
            ProcessLike(task, userLike);
            return task.Likes.Count;
        }

        /// <summary>
        /// Remove task by it's unique identifier.
        /// </summary>
        /// <param name="taskId">Unique identifier of task to be removed.</param>
        /// <exception cref="TaskEditException">Thrown if task cannot be edited anymore.</exception>
        /// <exception cref="TaskNotFoundException">Thrown if task was not found in database.</exception>
        /// <exception cref="DatabaseFailedException">Thrown if task are errors in database.</exception>
        /// <exception cref="NoRightsException">Thrown if user have no rights to perform current action.</exception>
        public void Remove(Guid taskId)
        {
            Task task = FindStrict<TaskNotFoundException>(taskId, SearchState.ItemNotFound);
            CheckUser(OperationAccess.Owner, task.Author.Login);
            CheckIfTaskIsEditable(task);
            RemoveAllTaskData(task);
        }

        private void RemoveAllTaskData(Task task)
        {
            task.Tags.Clear();
            RemoveLikes(task);
            RemoveAnswers(task);
            Logics.Database.TaskRepository.AddOrUpdate(task);
            Logics.Database.TaskRepository.Remove(task);
        }

        private void RemoveLikes(Task task)
        {
            List<Like> likes = task.Likes.ToList();
            for (int i = 0; i < likes.Count; i++)
            {
                Logics.Database.LikeRepository.Remove(likes[i]);
            }
            task.Likes.Clear();
        }

        private void CheckIfTaskIsEditable(Task task)
        {
            var draft = Logics.Database.DraftRepository.FirstOrDefault(d => d.Original != null 
                && d.Original.Id == task.Id);
            if (task.UsersSolved.Count != 0 || draft != null)
            {
                throw new TaskEditException();
            }
        }

        private void RemoveAnswers(Task task)
        {
            List<Answer> answers = task.Answers.ToList();
            for (int i = 0; i < answers.Count; i++)
            {
                Logics.Database.AnswerRepository.Remove(answers[i]);
            }
            task.Answers.Clear();
        }

        private void CheckIfTaskAlreadyExists(Task task)
        {
            Task oldTask = Logics.TaskAC.FirstOrDefault(t => t.NameForLink == task.NameForLink);
            if (oldTask != null)
            {
                throw new TaskAlreadyExistsException();
            }
        }

        private Task GetTask(String description, String text, String name,
                             String technology, String locale,
                             IEnumerable<String> tags, IEnumerable<String> answers)
        {
            return new Task()
                {
                    Id = Guid.NewGuid(),
                    Description = description,
                    Author = Logics.CurrentUser,
                    Language = Logics.LanguageAC.Find(locale, true),
                    Name = name,
                    Text = text,
                    Technology = Logics.TechnologyAC.GetWithName(technology),
                    PostDate = DateTime.UtcNow,
                    Tags = tags.Select(Logics.TagAC.GetWithName).ToList(),
                    Answers = answers.Select(Logics.AnswerAC.Create).ToList(),
                };
        }

        private Boolean AddIfAnswerIsCorrect(Task task, String answer)
        {
            IEnumerable<Answer> answers = task.Answers.Where(t => t.Text == answer);
            if (answers.Any())
            {
                Logics.CurrentUser.SolvedTasks.Add(task);
                Logics.Database.UserRepository.AddOrUpdate(Logics.CurrentUser);
            }
            return answers.Any();
        }

        private void CheckIfTaskIsAlreadySolved(Task task)
        {
            if (Logics.CurrentUser.SolvedTasks.Contains(task))
            {
                throw new TaskAlreadySolvedException();
            }
        }

        private void ProcessLike(Task task, Like userLike)
        {
            if (userLike != null)
            {
                Unlike(userLike);
            }
            else
            {
                CreateLike(task);
            }
        }

        private Like FindLike(Guid taskId, String login)
        {
            return Logics.Database.LikeRepository.FirstOrDefault(like => like.User.Login == login &&
                                                                         taskId == like.Task.Id);
        }

        private void Unlike(Like userLike)
        {
            Logics.Database.LikeRepository.Remove(userLike);
        }

        private void CreateLike(Task task)
        {
            Like newLike = new Like()
            {
                Id = Guid.NewGuid(),
                Task = task,
                User = Logics.CurrentUser,
            };
            Logics.Database.LikeRepository.Add(newLike);
        }
    }
}