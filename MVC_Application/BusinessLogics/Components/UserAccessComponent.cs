﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Web.Security;
using BL_Interfaces;
using BL_Interfaces.AccessComponents.Concrete;
using BL_Interfaces.Enumerations;
using BL_Interfaces.Exceptions;
using BusinessEntities;
using BusinessLogics.Helpers;
using BusinessLogics.Properties;
using MembershipRoutines;

namespace BusinessLogics.Components
{
    /// <summary>
    /// Class for access to operations with user's logics.
    /// </summary>
    public class UserAccessComponent : BasicAccessComponent<User>, IUserAccessComponent
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UserAccessComponent" /> class.
        /// </summary>
        /// <param name="logics">The logics.</param>
        public UserAccessComponent(ILogics logics)
            : base(logics)
        {
        }

        public void Edit(Guid id, string newLogin, string newPassword, string newEmail)
        {
            var userForEdit = FindStrict<LoginNotFoundException>(id, SearchState.ItemNotFound);
            CheckUser(OperationAccess.AlternativeRole | OperationAccess.Owner, userForEdit.Login,
                      Settings.Default.AdministratorRoleName);
            var userWithLogin = FindByLogin(newLogin, false);
            if (userWithLogin != null && userWithLogin.Login != newLogin)
            {
                throw new LoginAlreadyExistsException();
            }
            userForEdit.Login = newLogin;

            var userWithMail = FindByMail(newEmail, false);
            if (userWithMail != null && userWithMail.Email != newEmail)
            {
                throw new EmailAlreadyExistsException();
            }
            userForEdit.Email = newEmail;

            if (!string.IsNullOrEmpty(newPassword))
            {
                userForEdit.Password = Crypto.HashPassword(newPassword);
            }
            Logics.Database.UserRepository.AddOrUpdate(userForEdit);
        }

        /// <summary>
        /// Edits user with specified login.
        /// </summary>
        /// <param name="login">The login.</param>
        /// <param name="oldPassword">The old password.</param>
        /// <param name="newPassword">The new password.</param>
        /// <param name="newEmail">The new mail.</param>
        /// <exception cref="BL_Interfaces.Exceptions.IncorrectPasswordException"></exception>
        /// <exception cref="BL_Interfaces.Exceptions.EmailAlreadyExistsException"></exception>
        public void Edit(string login, string oldPassword, string newPassword, string newEmail)
        {
            CheckUser(OperationAccess.Owner, login);
            var user = FindByMail(newEmail, false);
            if (user != null && user.Email != newEmail)
            {
                throw new EmailAlreadyExistsException();
            }
            Logics.CurrentUser.Email = newEmail;
            var userForEdit = Membership.GetUser(login);
            try
            {
                userForEdit.ChangePassword(oldPassword, newPassword);
            }
            catch
            {
                throw new IncorrectPasswordException();
            }
            Logics.Database.UserRepository.AddOrUpdate(Logics.CurrentUser);
        }

        /// <summary>
        /// Registers user.
        /// </summary>
        /// <param name="login">The login.</param>
        /// <param name="password">The password.</param>
        /// <param name="email">The email.</param>
        /// <param name="isApproved">if set to <c>true</c> confirmation will be send of specified e-mail.</param>
        /// <exception cref="DatabaseFailedException">Thrown if there are errors in database.</exception>
        /// <exception cref="LoginAlreadyExistsException">Thrown if specified login already exists in database.</exception>
        /// <exception cref="EmailAlreadyExistsException">Thrown if specified e-mail already exists in database.</exception>
        public void Register(String login, String password, String email, Boolean isApproved)
        {
            CheckLogics();
            if (Membership.FindUsersByName(login).Count != 0)
            {
                throw new LoginAlreadyExistsException();
            }
            if (Membership.FindUsersByEmail(email).Count != 0)
            {
                throw new EmailAlreadyExistsException();
            }
            CreateUser(login, password, email, isApproved);
            Roles.AddUserToRole(login, Settings.Default.UserRoleName);
        }

        /// <summary>
        /// Finds user by mail.
        /// </summary>
        /// <param name="email">E-mail of user to be found.</param>
        /// <param name="throwExceptionIfNotFound">if set to <c>true</c> [throw exception if not found].</param>
        /// <returns>
        /// User or null (if exception is not thrown).
        /// </returns>
        /// <exception cref="DatabaseFailedException">Thrown if there are errors in database.</exception>
        /// <exception cref="EmailNotFoundException">Thrown if user with specified e-mail was not found in database.</exception>
        public User FindByMail(String email, Boolean throwExceptionIfNotFound)
        {
            CheckLogics();
            User user = Logics.Database.UserRepository.FindByEmail(email);
            if (user == null && throwExceptionIfNotFound)
            {
                throw new EmailNotFoundException();
            }
            return user;
        }

        /// <summary>
        /// Finds user by login.
        /// </summary>
        /// <param name="login">The login.</param>
        /// <param name="throwExceptionIfNotFound">if set to <c>true</c> [throw exception if not found].</param>
        /// <returns>User. </returns>
        /// <exception cref="DatabaseFailedException">Thrown if there are errors in database.</exception>
        /// <exception cref="LoginNotFoundException">Exception is thrown when user with inputted login was not found in database.</exception>
        public User FindByLogin(String login, Boolean throwExceptionIfNotFound)
        {
            CheckLogics();
            User user = Logics.Database.UserRepository.FindByLogin(login);
            if (user == null && throwExceptionIfNotFound)
            {
                throw new LoginNotFoundException();
            }
            return Logics.Database.UserRepository.FindByLogin(login);
        }

        /// <summary>
        /// Sends the verification.
        /// </summary>
        /// <param name="login">Login of user to be verified.</param>
        /// <exception cref="UserIsAlreadyApprovedException">Thrown if user is already approved.</exception>
        /// <exception cref="DatabaseFailedException">Thrown if there are errors in database.</exception>
        /// <exception cref="LoginNotFoundException">Exception is thrown when user with inputted login was not found in database.</exception>
        /// <exception cref="VerificationSendException">Thrown if errors occured while sending letter with verification.</exception>
        public void SendVerification(String login)
        {
            User user = FindByLogin(login, true);
            if (user.IsApproved)
            {
                throw new UserIsAlreadyApprovedException();
            }
            String body = String.Format("Your activation code is: {0}", user.ConfirmationToken);
            EmailConfirmationSender.SendMessage(user, body, "Thanks for registration!");
        }

        /// <summary>
        /// Approves user with specified login.
        /// </summary>
        /// <param name="login">Login of user to be approved.</param>
        /// <param name="confirmationToken">The confirmation token.</param>
        /// <returns>
        /// True if user was successfully approved.
        /// </returns>
        /// <exception cref="DatabaseFailedException">Thrown if there are errors in database.</exception>
        /// <exception cref="LoginNotFoundException">Exception is thrown when user with inputted login was not found in database.</exception>
        public Boolean Approve(String login, String confirmationToken)
        {
            User user = FindByLogin(login, true);
            if (!user.IsApproved && user.ConfirmationToken == confirmationToken)
            {
                user.IsApproved = true;
                Logics.Database.UserRepository.AddOrUpdate(user);
            }
            return user.IsApproved;
        }

        /// <summary>
        /// Determines whether user is in the specified role.
        /// </summary>
        /// <param name="role">Role to be checked.</param>
        /// <returns>
        /// <c>true</c> if user is in the specified role; otherwise, <c>false</c>.
        /// </returns>
        public Boolean IsInRole(String role)
        {
            CheckLogics();
            return Logics.CurrentUser != null && Roles.IsUserInRole(Logics.CurrentUser.Login, role);
        }

        /// <summary>
        /// Sets the language of specified user.
        /// </summary>
        /// <param name="login">Login of user that changes his language.</param>
        /// <param name="languageLocale">Language locale.</param>
        /// <exception cref="DatabaseFailedException">Thrown if there are errors in database.</exception>
        /// <exception cref="LoginNotFoundException">Exception is thrown when user with inputted login was not found in database.</exception>
        /// <exception cref="LanguageNotFoundException">Thrown if language was not found in database.</exception>
        public void SetLanguage(String login, String languageLocale)
        {
            User user = FindByLogin(login, true);
            Language language = Logics.LanguageAC.Find(languageLocale, false)
                ?? Logics.LanguageAC.FirstOrDefault(l => l.Name == languageLocale);
            if (language == null)
            {
                throw new LanguageNotFoundException();
            }
            SetLanguageAndSave(user, language);
        }

        /// <summary>
        /// Removes user with specified login.
        /// </summary>
        /// <param name="login">Login of user to be removed.</param>
        /// <exception cref="NoRightsException">Exception is thrown when user have no rights to perform some action.</exception>
        /// <exception cref="DatabaseFailedException">Thrown if there are errors in database.</exception>
        /// <exception cref="LoginNotFoundException">Exception is thrown when user with inputted login was not found in database.</exception>
        public void RemoveByUser(String login)
        {
            CheckUser(OperationAccess.Owner, login);
            User user = FindByLogin(login, true);
            user.IsRemoved = true;
            Logics.Database.UserRepository.AddOrUpdate(user);
        }

        /// <summary>
        /// Recovers user with specified login.
        /// </summary>
        /// <param name="login">The login.</param>
        /// <param name="password">The password.</param>
        /// <exception cref="IncorrectPasswordException"></exception>
        public void RecoverByUser(String login, string password)
        {
            var user = FindByLogin(login, true);
            if (Membership.ValidateUser(login, password))
            {
                user.IsRemoved = false;
                Logics.Database.UserRepository.AddOrUpdate(user);
            }
            else
            {
                throw new IncorrectPasswordException();
            }
        }

        /// <summary>
        /// Removes or recovers user by admin. (currently logged in user must have administrator's rights)
        /// </summary>
        /// <param name="login">The login.</param>
        public void RemoveOrRecoverByAdmin(String login)
        {
            var user = FindByLogin(login, true);
            CheckUser(OperationAccess.AlternativeRole, null, Settings.Default.AdministratorRoleName);
            user.IsRemoved = !user.IsRemoved;
            Logics.Database.UserRepository.AddOrUpdate(user);
        }

        /// <summary>
        /// Logs user in.
        /// </summary>
        /// <param name="login">Login of user to be logged in.</param>
        /// <param name="password">Password of user to be logged in.</param>
        /// <exception cref="AuthorizationFailedExpcetion">Exception is thrown when password or login are incorrect.</exception>
        /// <exception cref="DatabaseFailedException">Thrown if there are errors in database.</exception>
        /// <exception cref="LoginNotFoundException">Exception is thrown when user with inputted login was not found in database.</exception>
        public void LogIn(String login, String password)
        {
            User user = FindByLogin(login, true);
            if (Membership.ValidateUser(login, password))
            {
                Logics.CurrentUser = user;
            }
            else
            {
                throw new AuthorizationFailedExpcetion();
            }
        }

        /// <summary>
        /// Logs user out.
        /// </summary>
        /// <exception cref="DatabaseFailedException">Thrown if there are errors in database.</exception>
        public void LogOut()
        {
            CheckLogics();
            Logics.CurrentUser = null;
        }

        private void SetLanguageAndSave(User user, Language language)
        {
            user.Language = language;
            Logics.Database.UserRepository.AddOrUpdate(user);
        }

        private void CreateUser(String login, String password, String email, Boolean isApproved)
        {
            MembershipCreateStatus status;
            Membership.CreateUser(login, password, email, null, null, isApproved, null, out status);
            if (status == MembershipCreateStatus.Success)
            {
                Roles.AddUserToRole(login, Settings.Default.UserRoleName);
                if (!isApproved)
                {
                    SendVerification(login);
                }
            }
            else
            {
                throw new UnhandledRegistrationException(status.ToString());
            }
        }
    }
}