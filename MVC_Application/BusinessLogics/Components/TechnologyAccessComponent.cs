﻿using System;
using System.Collections.Generic;
using System.Linq;
using BL_Interfaces;
using BL_Interfaces.AccessComponents.Concrete;
using BL_Interfaces.Exceptions;
using BusinessEntities;

namespace BusinessLogics.Components
{
    /// <summary>
    /// Class for access to operations with technology's logics.
    /// </summary>
    public class TechnologyAccessComponent: BasicAccessComponent<Technology>, ITechnologyAccessComponent
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TechnologyAccessComponent" /> class.
        /// </summary>
        /// <param name="logics">The logics.</param>
        public TechnologyAccessComponent(ILogics logics) : base(logics)
        {
        }

        /// <summary>
        /// Cleans up (Removes all unused technologies from database).
        /// </summary>
        /// /// <exception cref="DatabaseFailedException">Thrown if there are errors in database.</exception>
        public void CleanUp()
        {
            CheckLogics();
            List<Technology> unusedTechnologies = Logics.Database.TechnologyRepository
                .Where(tech => tech.Drafts.Count == 0 && tech.Tasks.Count == 0).ToList();
            for (int i = 0; i < unusedTechnologies.Count; i++)
            {
                Logics.Database.TechnologyRepository.Remove(unusedTechnologies[i]);
            }
        }

        /// <summary>
        /// Get list of technologies that have name field containing inputted subString.
        /// </summary>
        /// <param name="subString">SubString used to get technologies.</param>
        /// <returns>
        /// List of technologies.
        /// </returns>
        /// <exception cref="DatabaseFailedException">Thrown if there are errors in database.</exception>
        public IEnumerable<Technology> FindTechnologiesContainingText(String subString)
        {
            CheckLogics();
            return Logics.Database.TechnologyRepository.Where(tech => tech.Name.Contains(subString));
        }

        /// <summary>
        /// Get technology from database. If no technology was not found - new technology will be created.
        /// </summary>
        /// <param name="name">Name of technology.</param>
        /// <returns>Technology with inputted name or newly created.</returns>
        /// <exception cref="DatabaseFailedException">Thrown if there are errors in database.</exception>
        public Technology GetWithName(String name)
        {
            CheckLogics();
            return FindByName(name) ?? CreateByName(name);
        }

        private Technology FindByName(String name)
        {
            return Logics.Database.TechnologyRepository.FirstOrDefault(tech => tech.Name == name);
        }

        private Technology CreateByName(String name)
        {
            Technology tech = CreateNew(name);
            Logics.Database.TechnologyRepository.Add(tech);
            return tech;
        }

        private static Technology CreateNew(String name)
        {
            Technology tech = new Technology
            {
                Id = Guid.NewGuid(),
                Name = name,
            };
            return tech;
        }
    }
}