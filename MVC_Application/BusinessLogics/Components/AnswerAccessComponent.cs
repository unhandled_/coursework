﻿using System;
using System.Web.Security;
using BL_Interfaces;
using BL_Interfaces.AccessComponents.Concrete;
using BL_Interfaces.Exceptions;
using BusinessEntities;

namespace BusinessLogics.Components
{
    /// <summary>
    /// Class for access to operations with answer's logics.
    /// </summary>
    public class AnswerAccessComponent : BasicAccessComponent<Answer>, IAnswerAccessComponent
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AnswerAccessComponent" /> class.
        /// </summary>
        /// <param name="logics">The logics.</param>
        public AnswerAccessComponent(ILogics logics)
            : base(logics)
        {
        }

        /// <summary>
        /// Creates new answer in database with specified text.
        /// </summary>
        /// <param name="text">Text of answer.</param>
        /// <returns>
        /// New answer.
        /// </returns>
        /// <exception cref="DatabaseFailedException">Thrown if there are errors in database.</exception>
        public Answer Create(String text)
        {
            CheckLogics();
            return new Answer {Id = Guid.NewGuid(), Text = text};
        }
    }
}