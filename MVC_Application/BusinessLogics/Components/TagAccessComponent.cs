﻿using System;
using System.Collections.Generic;
using System.Linq;
using BL_Interfaces;
using BL_Interfaces.AccessComponents.Concrete;
using BL_Interfaces.Exceptions;
using BusinessEntities;

namespace BusinessLogics.Components
{
    /// <summary>
    /// Class for access to operations with tags's logics.
    /// </summary>
    public class TagAccessComponent : BasicAccessComponent<Tag>, ITagAccessComponent
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TagAccessComponent" /> class.
        /// </summary>
        /// <param name="logics">The logics.</param>
        public TagAccessComponent(ILogics logics) : base(logics)
        {
        }

        /// <summary>
        /// Cleans up (Removes all unused tags from database).
        /// </summary>
        /// <exception cref="DatabaseFailedException">Thrown if there are errors in database.</exception>
        public void CleanUp()
        {
            CheckLogics();
            List<Tag> unusedTags = Logics.Database.TagRepository
                .Where(tag => tag.Tasks.Count == 0 && tag.Drafts.Count == 0).ToList();
            for (int i = 0; i < unusedTags.Count; i++)
            {
                Logics.Database.TagRepository.Remove(unusedTags[i]);
            }
        }

        /// <summary>
        /// Get tag from database. If no tag in database with inputted name was found - new tag is created.
        /// </summary>
        /// <param name="name">Name of tag.</param>
        /// <returns>
        /// Tag from database or newly created.
        /// </returns>
        /// <exception cref="DatabaseFailedException">Thrown if there are errors in database.</exception>
        public Tag GetWithName(String name)
        {
            return Find(name) ?? Create(name);
        }

        /// <summary>
        /// Find tag with by it's text.
        /// </summary>
        /// <param name="text">Text of tag to be found.</param>
        /// <returns>
        /// Tag or null.
        /// </returns>
        public Tag Find(String text)
        {
            CheckLogics();
            return Logics.Database.TagRepository.FirstOrDefault(tag => tag.Text == text);
        }

        /// <summary>
        /// Get list of tags that have text field containing inputted subString.
        /// </summary>
        /// <param name="subString">SubString used to get tags.</param>
        /// <returns>
        /// List of tags.
        /// </returns>
        /// <exception cref="DatabaseFailedException">Thrown if there are errors in database.</exception>
        public IEnumerable<Tag> FindTagsContainingText(String subString)
        {
            CheckLogics();
            return Logics.Database.TagRepository.Where(tag => tag.Text.Contains(subString));
        }

        /// <summary>
        /// Get list of most popular tags.
        /// </summary>
        /// <param name="tagsCount">Number of tags to be selected.</param>
        /// <param name="languageLocale">Language of tags.</param>
        /// <returns>
        /// List of most popular tags.
        /// </returns>
        /// <exception cref="DatabaseFailedException">Thrown if there are errors in database.</exception>
        public IEnumerable<Tag> GetMostPopular(int tagsCount, String languageLocale)
        {
            CheckLogics();
            var tags = Logics.Database.TagRepository.Where(tag => tag.Tasks.Count != 0);
            Dictionary<Tag, int> result = new Dictionary<Tag, int>();
            foreach(var tag in tags)
            {
                var tasks = tag.Tasks.Where(t => t.Language != null && t.Language.Locale == languageLocale);
                if (tasks.Count() != 0)
                {
                    var sum = tasks.Sum(t => t.Likes.Count);
                    result.Add(tag, sum);
                }
            }

            return result.OrderByDescending(t => t.Value).Select(t => t.Key).Take(tagsCount);
        }

        private Tag Create(String name)
        {
            CheckLogics();
            Tag tag = new Tag
            {
                Id = Guid.NewGuid(),
                Text = name,
            };
            Logics.Database.TagRepository.Add(tag);
            return tag;
        }
    }
}