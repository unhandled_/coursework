using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text.RegularExpressions;

namespace BusinessEntities
{
    /// <summary>
    /// Class to represent some task.
    /// </summary>
    public class Task
    {
        /// <summary>
        /// Default parameterless constructor.
        /// </summary>
        public Task()
        {
            Likes = new List<Like>();
            Tags = new List<Tag>();
            Answers = new List<Answer>();
            UsersSolved = new List<User>();
        }

        /// <summary>
        /// Unique identifier of current task.
        /// </summary>
        [Key]
        public virtual Guid Id { get; set; }

        /// <summary>
        /// Author of current task.
        /// </summary>
        public virtual User Author { get; set; }
        
        /// <summary>
        /// List of likes for current task.
        /// </summary>
        public virtual IList<Like> Likes { get; set; }

        /// <summary>
        /// List of tags for current task.
        /// </summary>
        public virtual IList<Tag> Tags { get; set; }

        /// <summary>
        /// When task was posted for the first time.
        /// </summary>
        public virtual DateTime PostDate { get; set; }

        /// <summary>
        /// List of correct answers for current task.
        /// </summary>
        public virtual IList<Answer> Answers { get; set; }

        /// <summary>
        /// Language of current task.
        /// </summary>
        public virtual Language Language { get; set; }

        /// <summary>
        /// Technology that is used is current task.
        /// </summary>
        public virtual Technology Technology { get; set; }

        /// <summary>
        /// Description of current task.
        /// </summary>
        public virtual String Description { get; set; }

        /// <summary>
        /// Text of current task.
        /// </summary>
        public virtual String Text { get; set; }

        /// <summary>
        /// Name of current task.
        /// </summary>
        public virtual String Name { get; set; }

        /// <summary>
        /// List of users that solved current task.
        /// </summary>
        public virtual IList<User> UsersSolved { get; set; }

        /// <summary>
        /// Transliterated and preprocessed name for access to task by it.
        /// </summary>
        [NotMapped]
        public String NameForLink
        {
            get
            {
                String clearedString = Regex.Replace(Name, @"\*|@|\^|;|<|>|\(|\)|\&|\?|%|\s|\\|\/|\=|\.|,|:", "_");
                return Regex.Replace(clearedString, "_+", "_").Trim('_').ToLower();
            }
        }
    }
}