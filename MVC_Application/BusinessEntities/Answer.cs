﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace BusinessEntities
{
    /// <summary>
    /// Class to represent answer to the task.
    /// </summary>
    public class Answer
    {
        /// <summary>
        /// Unique identifier of current answer.
        /// </summary>
        [Key]
        public virtual Guid Id { get; set; }

        /// <summary>
        /// Text of current answer.
        /// </summary>
        public virtual String Text { get; set; }
    }
}