using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BusinessEntities
{
    /// <summary>
    /// Class representation for role that user has.
    /// </summary>
    public class Role
    {
        /// <summary>
        /// Unique identifier of current user role.
        /// </summary>
        [Key]
        public virtual Guid RoleId { get; set; }

        /// <summary>
        /// Name of current user role.
        /// </summary>
        [Required]
        public virtual String RoleName { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>
        /// The description of role.
        /// </value>
        public virtual String Description { get; set; }

        /// <summary>
        /// Gets or sets list of users with current role.
        /// </summary>
        /// <value>
        /// List of users that has current role.
        /// </value>
        public virtual ICollection<User> Users { get; set; }
    }
}