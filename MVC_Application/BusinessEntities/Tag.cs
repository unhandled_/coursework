using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace BusinessEntities
{
    /// <summary>
    /// Class to represent tags of tasks and drafts.
    /// </summary>
    public class Tag
    {
        /// <summary>
        /// Default parameterless constructor.
        /// </summary>
        public Tag()
        {
            Tasks = new List<Task>();
            Drafts = new List<Draft>();
        }

        /// <summary>
        /// Unique identifier of current tag.
        /// </summary>
        [Key]
        public virtual Guid Id { get; set; }

        /// <summary>
        /// List of tasks that have referencies to current tag.
        /// </summary>
        public virtual IList<Task> Tasks { get; set; }

        /// <summary>
        /// List drafts that have referencies to current tag.
        /// </summary>
        public virtual IList<Draft> Drafts { get; set; }

        /// <summary>
        /// Text of current tag.
        /// </summary>
        public virtual String Text { get; set; }
    }
}