using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BusinessEntities
{
    /// <summary>
    /// Class to represent technology that was used in tasks.
    /// </summary>
    public class Technology
    {
        /// <summary>
        /// Default parameterless constructor.
        /// </summary>
        public Technology()
        {
            Tasks = new List<Task>();
            Drafts = new List<Draft>();
        }

        /// <summary>
        /// Unique identifier of current technology.
        /// </summary>
        [Key]
        public virtual Guid Id { get; set; }

        /// <summary>
        /// Name of current technology.
        /// </summary>
        public virtual String Name { get; set; }

        /// <summary>
        /// List of tasks that use current technology.
        /// </summary>
        public virtual IList<Task> Tasks { get; set; }

        /// <summary>
        /// List of drafts that use current technology.
        /// </summary>
        public virtual IList<Draft> Drafts { get; set; } 
    }
}