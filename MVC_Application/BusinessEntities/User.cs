using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace BusinessEntities
{
    /// <summary>
    /// Class to represent user.
    /// </summary>
    public class User
    {
        /// <summary>
        /// Default parameterless constuctor.
        /// </summary>
        public User()
        {
            Tasks = new List<Task>();
            Drafts = new List<Draft>();
            Likes = new List<Like>();
            SolvedTasks = new List<Task>();
            Roles = new Collection<Role>();
        }

        /// <summary>
        /// Login of current user.
        /// </summary>
        public virtual String Login { get; set; }

        /// <summary>
        /// E-mail of current user.
        /// </summary>
        public virtual String Email { get; set; }

        /// <summary>
        /// Language that current user set to default.
        /// </summary>
        public virtual Language Language { get; set; }

        /// <summary>
        /// List of tasks that were created by current user.
        /// </summary>
        public virtual IList<Task> Tasks { get; set; }

        /// <summary>
        /// List of drafts that were created by current user.
        /// </summary>
        public virtual IList<Draft> Drafts { get; set; }

        /// <summary>
        /// List of likes that user made to other tasks.
        /// </summary>
        public virtual IList<Like> Likes { get; set; }

        /// <summary>
        /// List of tasks that were solved by current user.
        /// </summary>
        public virtual IList<Task> SolvedTasks { get; set; }

        /// <summary>
        /// Flag that determines whether current user is removed or not.
        /// </summary>
        public virtual bool IsRemoved { get; set; }

        /// <summary>
        /// Flag that determines whether current user approved his status or not.
        /// </summary>
        public virtual bool IsApproved { get; set; }

        /// <summary>
        /// Unique identifier of current user.
        /// </summary>
        [Key]
        public virtual Guid UserId { get; set; }

        /// <summary>
        /// Gets or sets the password.
        /// </summary>
        /// <value>
        /// The password.
        /// </value>
        public virtual String Password { get; set; }

        /// <summary>
        /// Gets or sets the first name.
        /// </summary>
        /// <value>
        /// The first name.
        /// </value>
        public virtual String FirstName { get; set; }

        /// <summary>
        /// Gets or sets the last name.
        /// </summary>
        /// <value>
        /// The last name.
        /// </value>
        public virtual String LastName { get; set; }

        /// <summary>
        /// Gets or sets the comment.
        /// </summary>
        /// <value>
        /// The comment.
        /// </value>
        [DataType(DataType.MultilineText)]
        public virtual String Comment { get; set; }

        /// <summary>
        /// Gets or sets the password failures since last success.
        /// </summary>
        /// <value>
        /// The password failures since last success.
        /// </value>
        public virtual int PasswordFailuresSinceLastSuccess { get; set; }

        /// <summary>
        /// Gets or sets the last password failure date.
        /// </summary>
        /// <value>
        /// The last password failure date.
        /// </value>
        public virtual DateTime? LastPasswordFailureDate { get; set; }

        /// <summary>
        /// Gets or sets the last activity date.
        /// </summary>
        /// <value>
        /// The last activity date.
        /// </value>
        public virtual DateTime? LastActivityDate { get; set; }

        /// <summary>
        /// Gets or sets the last lockout date.
        /// </summary>
        /// <value>
        /// The last lockout date.
        /// </value>
        public virtual DateTime? LastLockoutDate { get; set; }

        /// <summary>
        /// Gets or sets the last login date.
        /// </summary>
        /// <value>
        /// The last login date.
        /// </value>
        public virtual DateTime? LastLoginDate { get; set; }

        /// <summary>
        /// Gets or sets the confirmation token.
        /// </summary>
        /// <value>
        /// The confirmation token.
        /// </value>
        public virtual String ConfirmationToken { get; set; }

        /// <summary>
        /// Gets or sets the create date.
        /// </summary>
        /// <value>
        /// The create date.
        /// </value>
        public virtual DateTime? CreateDate { get; set; }

        /// <summary>
        /// Gets or sets whether user is locked out.
        /// </summary>
        /// <value>
        /// Is user locked out.
        /// </value>
        public virtual Boolean IsLockedOut { get; set; }

        /// <summary>
        /// Gets or sets the last password changed date.
        /// </summary>
        /// <value>
        /// The last password changed date.
        /// </value>
        public virtual DateTime? LastPasswordChangedDate { get; set; }

        /// <summary>
        /// Gets or sets the password verification token.
        /// </summary>
        /// <value>
        /// The password verification token.
        /// </value>
        public virtual String PasswordVerificationToken { get; set; }

        /// <summary>
        /// Gets or sets the password verification token expiration date.
        /// </summary>
        /// <value>
        /// The password verification token expiration date.
        /// </value>
        public virtual DateTime? PasswordVerificationTokenExpirationDate { get; set; }

        /// <summary>
        /// Gets or sets the roles.
        /// </summary>
        /// <value>
        /// Roles that user has in application.
        /// </value>
        public virtual ICollection<Role> Roles { get; set; }
    }
}