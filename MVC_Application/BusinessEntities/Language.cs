using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BusinessEntities
{
    /// <summary>
    /// Class to represent language of some data.
    /// </summary>
    public class Language
    {
        public Language()
        {
            Users = new List<User>();
            Tasks = new List<Task>();
            Drafts = new List<Draft>();
        }

        /// <summary>
        /// Unique identifier of current language.
        /// </summary>
        [Key]
        public virtual Guid Id { get; set; }

        /// <summary>
        /// Short name of language.
        /// </summary>
        public virtual String Locale { get; set; }

        /// <summary>
        /// Name of language.
        /// </summary>
        public virtual String Name { get; set; }

        /// <summary>
        /// List of users that use current language.
        /// </summary>
        public virtual IList<User> Users { get; set; }

        /// <summary>
        /// List of task in current language.
        /// </summary>
        public virtual IList<Task> Tasks { get; set; }

        /// <summary>
        /// List of drafts in current language.
        /// </summary>
        public virtual IList<Draft> Drafts { get; set; } 
    }
}