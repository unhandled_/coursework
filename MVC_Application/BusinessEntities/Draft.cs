using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text.RegularExpressions;

namespace BusinessEntities
{
    /// <summary>
    /// Class to represent draft for some task.
    /// </summary>
    public class Draft
    {
        public Draft()
        {
            Tags = new List<Tag>();
            Answers = new List<Answer>();
        }

        /// <summary>
        /// Unique identifier of current draft.
        /// </summary>
        [Key]
        public virtual Guid Id { get; set; }

        /// <summary>
        /// Author of current draft.
        /// </summary>
        public virtual User Author { get; set; }

        /// <summary>
        /// List of tags of current draft.
        /// </summary>
        public virtual IList<Tag> Tags { get; set; }

        /// <summary>
        /// List of answers for current draft.
        /// </summary>
        public virtual IList<Answer> Answers { get; set; }

        /// <summary>
        /// Time of last modification of current draft.
        /// </summary>
        public virtual DateTime LastModified { get; set; }

        /// <summary>
        /// Task that is original of current draft.
        /// </summary>
        public virtual Task Original { get; set; }

        /// <summary>
        /// Text of current draft.
        /// </summary>
        public virtual String Text { get; set; }

        /// <summary>
        /// Name of current draft.
        /// </summary>
        public virtual String Name { get; set; }

        /// <summary>
        /// Language of current draft.
        /// </summary>
        public virtual Language Language { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>
        /// The description of current task.
        /// </value>
        public virtual String Description { get; set; }


        /// <summary>
        /// Gets or sets the technology.
        /// </summary>
        /// <value>
        /// The technology of current draft.
        /// </value>
        public virtual Technology Technology { get; set; }

        /// <summary>
        /// Transliterated and preprocessed name for access to draft by it.
        /// </summary>
        [NotMapped]
        public String NameForLink
        {
            get
            {
                String clearedString = Regex.Replace(Name, @"\*|@|\^|;|<|>|\(|\)|\&|\?|%|\s|\\|\/|\=|\.|,|:", "_");
                return Regex.Replace(clearedString, "_+", "_").Trim('_').ToLower();
            }
        }
    }
}