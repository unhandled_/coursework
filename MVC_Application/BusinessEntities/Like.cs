using System;
using System.ComponentModel.DataAnnotations;

namespace BusinessEntities
{
    /// <summary>
    /// Class to represent likes fromm users to tasks.
    /// </summary>
    public class Like
    {
        /// <summary>
        /// Unique identifier of current like.
        /// </summary>
        [Key]
        public virtual Guid Id { get; set; }

        /// <summary>
        /// User which pressed like to current task.
        /// </summary>
        public virtual User User { get; set; }

        /// <summary>
        /// That that was liked by user.
        /// </summary>
        public virtual Task Task { get; set; }
    }
}