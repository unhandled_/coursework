using System;
using System.Collections.Generic;

namespace DAL_Interfaces.Repositories.Basic
{
    /// <summary>
    /// Interface giving operations with database.
    /// </summary>
    /// <typeparam name="ItemType">Type of items to be processed by current repository.</typeparam>
    public interface IRepository<ItemType>
    {
        /// <summary>
        /// Gets the database.
        /// </summary>
        /// <value>
        /// The database.
        /// </value>
        IDatabase Database { get; }

        /// <summary>
        /// Insert item in database.
        /// </summary>
        /// <param name="item">Item to be added.</param>
        void Add(ItemType item);
        
        /// <summary>
        /// Insert or update item in database.
        /// </summary>
        /// <param name="item">Item to be added or updated.</param>
        void AddOrUpdate(ItemType item);
        
        /// <summary>
        /// Remove item from database.
        /// </summary>
        /// <param name="item">Item to be removed.</param>
        void Remove(ItemType item);

        /// <summary>
        /// Find item in database by it's unique identifier.
        /// </summary>
        /// <param name="id">Item's unique identifier.</param>
        /// <returns>Item form database if it was found, otherwise - null.</returns>
        ItemType Find(Guid id);

        /// <summary>
        /// Filters sequence based on selector.
        /// </summary>
        /// <param name="selector">Function to test each element for condition.</param>
        /// <returns>List of filtered items from database.</returns>
        IEnumerable<ItemType> Where(Func<ItemType, bool> selector);

        /// <summary>
        /// Select item from sequence based on selector.
        /// </summary>
        /// <param name="selector">Function to transform each element.</param>
        /// <returns>List of selected items from database.</returns>
        IEnumerable<TResult> Select<TResult>(Func<ItemType, TResult> selector);

        /// <summary>
        /// Select all elements from sequence.
        /// </summary>
        /// <returns>List of items.</returns>
        IEnumerable<ItemType> All();

        /// <summary>
        /// Get count of elements in database.
        /// </summary>
        /// <returns>Count of elements.</returns>
        int Count();

        /// <summary>
        /// Get count of elements filtered from database.
        /// </summary>
        /// <param name="selector">Function to test each element for condition.</param>
        /// <returns>Count of filtered elements in sequence.</returns>
        int Count(Func<ItemType, bool> selector);

        /// <summary>
        /// Get first element in sequence or null of it's not found.
        /// </summary>
        /// <param name="selector">Function to test each element for condition.</param>
        /// <returns>First element or null if it's not found.</returns>
        ItemType FirstOrDefault(Func<ItemType, bool> selector);
    }
}