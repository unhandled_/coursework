using System;
using BusinessEntities;
using DAL_Interfaces.Repositories.Basic;

namespace DAL_Interfaces.Repositories.Concrete
{    
    /// <summary>
    /// Interface giving operations with users.
    /// </summary>
    public interface IUserRepository: IRepository<User>
    {
        /// <summary>
        /// Find user with required login.
        /// </summary>
        /// <param name="login">Login of user to be found.</param>
        /// <returns>User with current login or null.</returns>
        User FindByLogin(String login);

        /// <summary>
        /// Find user with required e-mail.
        /// </summary>
        /// <param name="email">E-mail of user to be found.</param>
        /// <returns>User with current e-mail or null.</returns>
        User FindByEmail(String email);
    }
}