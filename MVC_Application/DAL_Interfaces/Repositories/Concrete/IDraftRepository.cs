using BusinessEntities;
using DAL_Interfaces.Repositories.Basic;

namespace DAL_Interfaces.Repositories.Concrete
{
    /// <summary>
    /// Interface giving operations with drafts.
    /// </summary>
    public interface IDraftRepository: IRepository<Draft>
    {
        
    }
}