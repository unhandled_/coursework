using BusinessEntities;
using DAL_Interfaces.Repositories.Basic;

namespace DAL_Interfaces.Repositories.Concrete
{
    /// <summary>
    /// Interface giving operations with likes.
    /// </summary>
    public interface ILikeRepository: IRepository<Like>
    {
        
    }
}