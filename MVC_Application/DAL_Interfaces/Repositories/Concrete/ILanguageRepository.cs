using System;
using BusinessEntities;
using DAL_Interfaces.Repositories.Basic;

namespace DAL_Interfaces.Repositories.Concrete
{
    /// <summary>
    /// Interface giving operations with languages.
    /// </summary>
    public interface ILanguageRepository: IRepository<Language>
    {
        /// <summary>
        /// Find language by it's locale name;
        /// </summary>
        /// <param name="localeName">Locale name of language.</param>
        /// <returns>Language with current locale or null.</returns>
        Language Find(String localeName);
    }
}