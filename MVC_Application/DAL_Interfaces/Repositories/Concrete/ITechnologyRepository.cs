using BusinessEntities;
using DAL_Interfaces.Repositories.Basic;

namespace DAL_Interfaces.Repositories.Concrete
{
    /// <summary>
    /// Interface giving operations with technologies
    /// </summary>
    public interface ITechnologyRepository: IRepository<Technology>
    {
        
    }
}