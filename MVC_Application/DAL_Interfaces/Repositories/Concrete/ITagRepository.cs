using BusinessEntities;
using DAL_Interfaces.Repositories.Basic;

namespace DAL_Interfaces.Repositories.Concrete
{
    /// <summary>
    /// Interface giving operations with tags.    
    /// </summary>
    public interface ITagRepository: IRepository<Tag>
    {
        
    }
}