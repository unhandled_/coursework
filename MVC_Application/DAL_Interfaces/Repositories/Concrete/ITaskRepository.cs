using System;
using System.Collections.Generic;
using BusinessEntities;
using DAL_Interfaces.Repositories.Basic;

namespace DAL_Interfaces.Repositories.Concrete
{
    /// <summary>
    /// Interface giving operations with tasks.
    /// </summary>
    public interface ITaskRepository: IRepository<Task>
    {
        IEnumerable<Task> Find(String query);
            

        /// <summary>
        /// Find task by it's link name.
        /// </summary>
        /// <param name="linkName">Name of task for links.</param>
        /// <returns>Task with current link name or null.</returns>
        Task FindByLinkName(String linkName);
    }
}