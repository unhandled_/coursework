using System;
using BusinessEntities;
using DAL_Interfaces.Repositories.Basic;

namespace DAL_Interfaces.Repositories.Concrete
{
    /// <summary>
    /// Interface giving operations with user roles.
    /// </summary>
    public interface IUserRoleRepository: IRepository<Role>
    {
        Role FindByName(String name);
    }
}