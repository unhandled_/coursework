﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL_Interfaces.Repositories.Basic;
using DAL_Interfaces.Repositories.Concrete;

namespace DAL_Interfaces
{
    /// <summary>
    /// Interface for database classes.
    /// </summary>
    public interface IDatabase: IDisposable
    {
        /// <summary>
        /// Interface giving access to operations with answers in database.
        /// </summary>
        IAnswerRepository AnswerRepository { get; set; }

        /// <summary>
        /// Interface giving access to operations with users in database.
        /// </summary>
        IUserRepository UserRepository { get; set; }

        /// <summary>
        /// Interface giving access to operations with user roles in database.
        /// </summary>
        /// <value>
        /// The user role repository.
        /// </value>
        IUserRoleRepository UserRoleRepository { get; set; }

        /// <summary>
        /// Interface giving access to operations with tags in database.
        /// </summary>
        ITagRepository TagRepository { get; set; }

        /// <summary>
        /// Interface giving access to operations with tasks in database.
        /// </summary>
        ITaskRepository TaskRepository { get; set; }

        /// <summary>
        /// Interface giving access to operations with technologies in database.
        /// </summary>
        ITechnologyRepository TechnologyRepository { get; set; }

        /// <summary>
        /// Interface giving access to operations with drafts in database.
        /// </summary>
        IDraftRepository DraftRepository { get; set; }

        /// <summary>
        /// Interface giving access to operations with likes in database.
        /// </summary>
        ILikeRepository LikeRepository { get; set; }

        /// <summary>
        /// Interface giving access to operations with languages in database.
        /// </summary>
        ILanguageRepository LanguageRepository { get; set; }

        /// <summary>
        /// Save changes to database.
        /// </summary>
        void SaveChanges();

        IRepository<ItemType> Repository<ItemType>();
    }
}
